﻿using System;
using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Date_studios : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from tbl_date_studios");

            pMysql.Message = "date_studios - extraction - START";
            int pcounter = 0;
            while (dataReader.Read())
            {
                pPostgres.Insert("insert into date_studios(id, tour_date_id, studio_id, " +
                                 "fees, " +
                                 "full_rates, independent, invoice_note, " +
                                 "free_teacher, registration_id, emailer_count, " +
                                 "studio_code, confirm_date) " +
                                 "values('"+dataReader["id"]+ "','" + dataReader["tourdateid"] + "','" + dataReader["studioid"] + "'," +
                                 "" + Get_json_fees(dataReader["total_fees"].ToString(), NVLJSON(dataReader["fees_paid"].ToString().Replace(",","")).ToString(), NVLJSON(dataReader["credit"].ToString().Replace("$","")).ToString()) + "," +
                                 "'" + CheckBool(dataReader["full_rates"].ToString()) + "','" + CheckBool(dataReader["independent"].ToString()) + "'," + NVL(dataReader["invoice_note"].ToString().Replace("'","''")) + "," +
                                 "" + Get_json_free_teacher(NVLJSON(dataReader["free_teacher_value"].ToString()), NVLJSON(dataReader["free_teacher_count"].ToString())) + "," +NVL(dataReader["statsregid"].ToString()) + "," +
                                 "'" + dataReader["emailer_count"] + "'," + NVL(dataReader["studiocode"].ToString()) + "," + FromUnixTimeQuotes(Convert.ToInt64(dataReader["confirmdate"])).ToString().Replace(". ", ".") + ")");
            }
            pPostgres.Message = "date_studios - extraction - FINISH";
        }

        private string Get_json_fees(string total, string paid, string credit)
        {
            dynamic fees = new JObject();
            fees.total = Math.Round(Convert.ToDecimal(NVLINT(total)), 2);
            fees.paid = Math.Round(Convert.ToDecimal(NVLINT(paid.Replace("\\","").Replace("h",""))), 2);
            fees.credit = Math.Round(Convert.ToDecimal(NVLINT(credit)), 2);
            return "'" + fees.ToString() + "'";
        }

        private string Get_json_free_teacher(string value, string count)
        {
            dynamic free_teacher = new JObject();
            free_teacher.value = Math.Round(Convert.ToDecimal(NVLINT(value)), 2);
            free_teacher.count = Convert.ToInt32(NVLINT(count));
            return "'" + free_teacher.ToString() + "'";
        }
    }
}