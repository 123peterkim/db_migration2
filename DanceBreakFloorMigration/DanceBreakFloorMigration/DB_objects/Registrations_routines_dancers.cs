﻿using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;
using System;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Registrations_routines_dancers : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from tbl_user_registrations_routines_dancers");
            pMysql.Message = "registrations_routines_dancers - extraction - START ";
            while (dataReader.Read())
            {
                int maxid = Convert.ToInt32(dataReader["regid"].ToString())+500000;

                string RegId = GetId("select id from tbl_registrations where id='" + maxid + "' limit 1;", pPostgres);

                pPostgres.Insert("insert into registrations_routines_dancers(id, registration_id, registrations_routine_id, dancer_id, workshop_skip) " +
                                 "values("+dataReader["id"]+", "+RegId+","+dataReader["regroutineid"] +","+dataReader["profileid"] +","+CheckBool(dataReader["workshopskip"].ToString()) + ");");
            }

            pPostgres.Message = "registrations_routines_dancers - extraction - FINISH";
        }
    }
}