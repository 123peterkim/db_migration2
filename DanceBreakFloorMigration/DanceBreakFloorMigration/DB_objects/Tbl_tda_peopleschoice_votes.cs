﻿using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_tda_peoples_choice_votes : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from tbl_tda_peopleschoice_votes;");
            pMysql.Message = "tbl_tda_peoples_choice_votes - extraction - START ";
            while (dataReader.Read())
            {
                pPostgres.Insert("insert into tbl_tda_peoples_choice_votes(id, tda_award_nomination_id, ip, routine_id, tour_date_id) " +
                                 "values("+dataReader["id"]+ "," + NVL(dataReader["nomid"].ToString()) + ",'" + dataReader["ip"] + "'," +
                                 "" + NVL(dataReader["routineid"].ToString()) + "," + NVL(dataReader["tourdateid"].ToString()) + ");");
            }

            pPostgres.Message = "tbl_tda_peoples_choice_votes - extraction - FINISH";
        }
    }
}