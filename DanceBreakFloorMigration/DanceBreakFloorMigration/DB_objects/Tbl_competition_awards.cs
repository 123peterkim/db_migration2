﻿using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_competition_awards : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from tbl_competition_awards;");

            pMysql.Message = "Tbl_competition_awards - extraction - START";
            int pcounter = 0;
            while (dataReader.Read())
            {
                string p_award_id = GetId("select id from tbl_award_types where name like '" + dataReader["name"].ToString().Replace("'", "''") + "' limit 1", pPostgres);

                pPostgres.Insert("insert into tbl_competition_awards(id, lowest, highest, award_type_id, event_id) " +
                                 "values("+dataReader["id"]+", "+dataReader["lowest"] +", "+dataReader["highest"] +","+ p_award_id + ","+dataReader["eventid"] +")");
            }
            pPostgres.Insert("insert into tbl_competition_awards(id) values(0);");
            pPostgres.Message = "Tbl_competition_awards - extraction - FINISH";
        }
    }
}