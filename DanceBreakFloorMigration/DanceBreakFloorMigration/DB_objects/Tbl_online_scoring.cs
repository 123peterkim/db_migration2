﻿using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_online_scoring : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from tbl_online_scoring");

            pMysql.Message = "tbl_online_scoring - extraction - START";
            while (dataReader.Read())
            {
                string competition_group_id = "null";
                string comp_group = dataReader["compgroup"].ToString();
                string tour_date_id = GetId("select event_id from tbl_tour_dates where id='" + dataReader["tourdateid"].ToString() + "' limit 1", pPostgres);

                if (comp_group == "finals")
                {
                    competition_group_id = "2";
                }

                if (comp_group == "prelims")
                {
                    competition_group_id = "1";
                }

                if (comp_group == "vips")
                {
                    if (tour_date_id == "14")
                    {
                        competition_group_id = "3";
                    } else
                    {
                        competition_group_id = "4";
                    }

                }
              
                pPostgres.Insert("insert into tbl_online_scoring(id, competition_award_id, tour_date_id, studio_id, competition_group_id, total_score, dropped_score, routine_id) " +
                                 "values('"+dataReader["id"]+ "','" + dataReader["awardid"] + "','" + dataReader["tourdateid"] + "','" + dataReader["studioid"] +"'," +
                                 "'" + competition_group_id + "','" + dataReader["total_score"] + "','" + dataReader["dropped_score"] + "','" + dataReader["routineid"] + "')");
            }
            pPostgres.Insert("insert into tbl_online_scoring_attributes(id, name) values (1, 'Control / Balance');");
            pPostgres.Insert("insert into tbl_online_scoring_attributes(id, name) values (2, 'Elevation');");
            pPostgres.Insert("insert into tbl_online_scoring_attributes(id, name) values (3, 'Turns');");
            pPostgres.Insert("insert into tbl_online_scoring_attributes(id, name) values (4, 'Arms');");
            pPostgres.Insert("insert into tbl_online_scoring_attributes(id, name) values (5, 'Hands');");
            pPostgres.Insert("insert into tbl_online_scoring_attributes(id, name) values (6, 'Extensions');");
            pPostgres.Insert("insert into tbl_online_scoring_attributes(id, name) values (7, 'Head / Shoulders');");
            pPostgres.Insert("insert into tbl_online_scoring_attributes(id, name) values (8, 'Body Lines');");
            pPostgres.Insert("insert into tbl_online_scoring_attributes(id, name) values (9, 'Feet');");
            pPostgres.Insert("insert into tbl_online_scoring_attributes(id, name) values (10, 'Legs');");
            pPostgres.Insert("insert into tbl_online_scoring_attributes(id, name) values (11, 'Personality');");
            pPostgres.Insert("insert into tbl_online_scoring_attributes(id, name) values (12, 'Stage Presence');");
            pPostgres.Insert("insert into tbl_online_scoring_attributes(id, name) values (13, 'Smile');");
            pPostgres.Insert("insert into tbl_online_scoring_attributes(id, name) values (14, 'Intensity');");
            pPostgres.Insert("insert into tbl_online_scoring_attributes(id, name) values (15, 'Emotion');");
            pPostgres.Insert("insert into tbl_online_scoring_attributes(id, name) values (16, 'Choreography');");
            pPostgres.Insert("insert into tbl_online_scoring_attributes(id, name) values (17, 'Precision');");
            pPostgres.Insert("insert into tbl_online_scoring_attributes(id, name) values (18, 'Creativeness');");
            pPostgres.Insert("insert into tbl_online_scoring_attributes(id, name) values (19, 'Timing');");
            pPostgres.Insert("insert into tbl_online_scoring_attributes(id, name) values (20, 'Difficulty');");
            pPostgres.Insert("insert into tbl_online_scoring_attributes(id, name) values (21, 'Use of Space');");
            pPostgres.Insert("insert into tbl_online_scoring_attributes(id, name) values (22, 'Continuity');");
            pPostgres.Insert("insert into tbl_online_scoring_attributes(id, name) values (23, 'Costume');");
            pPostgres.Insert("insert into tbl_online_scoring_attributes(id, name) values (24, 'Confidence');");
            pPostgres.Insert("insert into tbl_online_scoring_attributes(id, name) values (25, 'Appearance');");
            pPostgres.Insert("insert into tbl_online_scoring_attributes(id, name) values (26, 'Appropriateness');");
            pPostgres.Message = "tbl_online_scoring - extraction - FINISH";
        }
    }
}