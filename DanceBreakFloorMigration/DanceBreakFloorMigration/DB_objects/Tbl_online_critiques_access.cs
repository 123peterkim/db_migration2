﻿using System;
using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_online_critique_access_codes: BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from tbl_online_critiques_access;");
            pMysql.Message = "tbl_online_critique_access_codes - extraction - START";
            while (dataReader.Read())
            {
                    pPostgres.Insert("insert into tbl_online_critique_access_codes(id, tour_date_id, studio_id, code) " +
                                 "values('" + dataReader["id"] + "'," +
                                 "'" + dataReader["tourdateid"] + "','" + dataReader["studioid"] + "','" + dataReader["accesscode"] + "')");
            }
            pPostgres.Message = "tbl_online_critique_access_codes - extraction - FINISH";
        }
    }
}