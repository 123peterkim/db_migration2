﻿using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;
using System;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_registrations_dancers : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from tbl_user_registrations_dancers;");
            pMysql.Message = "tbl_registrations_dancers - extraction - START ";
            while (dataReader.Read())
            {
                int maxid = Convert.ToInt32(dataReader["regid"].ToString())+500000;

                string RegId = GetId("select id from tbl_registrations where id='" + maxid + "' limit 1;", pPostgres);

                pPostgres.Insert(
                    "insert into tbl_registrations_dancers(id, registration_id, dancer_id, one_day, attended_reg, is_commuter, " +
                    "non_commuter, classes_only, workshop_level_id, fees, promo_code_id, scholarship, best_dancer, event_teacher, event_id, event_reg_type_id) " +
                    "values(" + dataReader["id"] + "," + RegId + "," + dataReader["profileid"] + "," +
                    CheckBool(dataReader["oneday"].ToString()) + "," +
                    "" + CheckBool(dataReader["attendedreg"].ToString()) + ", " + CheckBool(dataReader["iscommuter"].ToString()) + "," + NVL(dataReader["noncommuterid"].ToString()) +
                    ", " + CheckBool(dataReader["classesonly"].ToString()) + ", " +
                    "" + dataReader["workshoplevelid"] + ",'" +
                    Get_json_fees(dataReader["workshopfee"].ToString(), dataReader["attendeefee"].ToString()) + "'," +
                    "" + NVL(dataReader["promocodeid"].ToString()) + ",'" +
                    Get_json_scholarship(dataReader["scholarshipamt"].ToString(),
                        dataReader["scholarshipfrom"].ToString().Replace("'", "''")) + "'," +
                    "" + CheckBool(dataReader["bestdancer"].ToString()) + ", " + CheckBool(dataReader["event_teacher"].ToString()) + "," + NVL(dataReader["intensiveid"].ToString()) +
                    "," + NVL(dataReader["regtypeid"].ToString()) + ");");
            }

            pPostgres.Message = "tbl_registrations_dancers - extraction - FINISH";
        }
        private string Get_json_fees(string pWorkshop, string pAttendee)
        {
            dynamic fees = new JObject();
            fees.workshop = Math.Round(Convert.ToDecimal(NVLINT(pWorkshop)), 2);
            fees.attendee = Math.Round(Convert.ToDecimal(NVLINT(pAttendee)), 2);
            return fees.ToString();
        }
        private string Get_json_scholarship(string pAmount, string pFrom)
        {
            dynamic scholarship = new JObject();

            if (pAmount == "half" || pAmount == "full")
            {
                scholarship.amount = pAmount;
            } else
            {
                scholarship.amount = Math.Round(Convert.ToDecimal(NVLINT(pAmount)), 2);
            }
            scholarship.from = NVLJSON(pFrom);
            return scholarship.ToString();
        }
    }
}