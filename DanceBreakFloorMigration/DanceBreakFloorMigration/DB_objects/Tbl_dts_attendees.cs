﻿using System;
using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_dts_attendees : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from tbl_dts_attendees;");
            pMysql.Message = "tbl_dts_attendees - extraction - START ";
            while (dataReader.Read())
            {
                if (!String.IsNullOrEmpty(dataReader["promocodeid"].ToString()))
                {
                    CreateDummyPromoCode(NVL(dataReader["promocodeid"].ToString()), pPostgres);
                }

                string pPersonId= AddNewPerson(dataReader["fname"].ToString(), dataReader["lname"].ToString(), dataReader["email"].ToString(), dataReader["title"].ToString(), pPostgres);

                pPostgres.Insert("insert into tbl_dts_attendees(id, fees, notes, canceled, person_id, dts_registration_id, promo_code_id, dts_reg_type_id) " +
                                 "values("+dataReader["id"]+ "," + Get_json_fee(NVLJSON(dataReader["fee"].ToString()), CheckBool(dataReader["custom_fee"].ToString())) + "," +
                                 "'" + dataReader["note"].ToString().Replace("'", "''") + "'," + CheckBool(dataReader["canceled"].ToString()) + ","+ pPersonId + ","+dataReader["registrationid"] +","+NVL(dataReader["promocodeid"].ToString()) + "," + dataReader["regtypeid"] + ");");
            }
            pPostgres.Message = "tbl_dts_attendees - extraction - FINISH";
        }

        public string AddNewPerson(string pName, string pLname, string pEmail, string pTitle, PostgreSQL_DB pPostgres)
        {
            string PersonType = GetId("select id from tbl_person_types where name like '" + pTitle + "' limit 1;", pPostgres);
            int Max_person_id = Convert.ToInt32(GetId("select max(id) from tbl_persons;", pPostgres));
            pPostgres.Insert("insert into tbl_persons(id, fname, lname, person_type_id) " +
                                 "values("+ ++Max_person_id +",'" + pName.ToString().Replace("'", "''") + "'," +
                                 "'" + pLname.ToString().Replace("'", "''") + "'," + PersonType + ");");

            if (!String.IsNullOrEmpty(pEmail.ToString()))
            {
                pPostgres.Insert("insert into person_contact_info(person_id, contact_type_id, value)" +
                                 "values(" + Max_person_id + ",1,'" + pEmail.ToLower().Replace("'", "''") + "');");
            }
            return Max_person_id.ToString();
        }

        private string Get_json_fee(string amount, int custom)
        {
            dynamic fee = new JObject();
            fee.amount = Math.Round(Convert.ToDecimal(NVLINT(amount)), 2);
            fee.custom = Math.Round(Convert.ToDecimal(NVLINT(custom.ToString())), 2);
            return "'" + fee.ToString() + "'";
        }
    }
}