﻿using System;
using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;
using Npgsql;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_dts_registrations : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from tbl_dts_registrations");
            pMysql.Message = "tbl_dts_registrations - extraction - START ";
            while (dataReader.Read())
            {
                string PersonId = GetPersonId(dataReader["address"].ToString().Replace("'", "''"), dataReader["city"].ToString().Replace("'", "''"),
                    dataReader["state"].ToString().Replace("'", "''"), dataReader["zip"].ToString(),
                    dataReader["countryid"].ToString(), dataReader["fname"].ToString().Replace("'", "''"), dataReader["lname"].ToString().Replace("'", "''"),
                    dataReader["email"].ToString(),
                    dataReader["contact_type"].ToString(), dataReader["phone"].ToString(),
                    dataReader["phone2"].ToString(), dataReader["fax"].ToString(),pPostgres);
                string studioid = GetStudioId(dataReader["organization"].ToString(),
                    pPostgres);

                pPostgres.Insert(
                    "insert into tbl_dts_registrations(id,tour_date_id,studio_id,person_id,registration_id,mybtf_user_id,admin_id," +
                    "heard,notes,fees,balance_due,boxsets, extra, edu, registration_date) " +
                    "values(" + dataReader["id"]+", "+NVL(dataReader["tourdateid"].ToString()) + ", " + studioid + ", " + PersonId + ", " +
                    "" +NVL(dataReader["onlineregid"].ToString()) + ", " +NVL(dataReader["onlineuserid"].ToString()) + ", " +NVL(dataReader["enteredbyid"].ToString()) + ", '" + dataReader["heard"].ToString().Replace("'","''") + "', " +
                    "'" + dataReader["notes"].ToString().Replace("'","''") + "', " + Get_json_fees(NVLJSON(dataReader["totalfees"].ToString()), NVLJSON(dataReader["feespaid"].ToString())) + ", " +NVL(dataReader["balancedue"].ToString()) + ", " +
                    "'" +Get_json_boxsets(dataReader["boxsets_2009"].ToString(), dataReader["boxsets_2010"].ToString(), dataReader["boxsets_0910_combo"].ToString())  + "'," +
                    "'" + Get_json_extra(dataReader["extra_dvdpre"].ToString(), dataReader["extra_gala"].ToString(), dataReader["extra_ff"].ToString(), dataReader["extra_ace"].ToString()) + "', " +
                    "'" + Get_json_edu(dataReader["edu_studioco"].ToString(), dataReader["edu_nostudioco"].ToString(), dataReader["edu_college"].ToString(),
                    dataReader["edu_k12"].ToString(), dataReader["edu_danceteam"].ToString(), dataReader["edu_other"].ToString(), dataReader["edu_other_val"].ToString()) + "',"+CheckIfUnix(dataReader["regdate"])+");")
                ;
            }
            pPostgres.Message = "tbl_dts_registrations - extraction - FINISH";

            MySqlDataReader dataReader2 = pMysql.Select("select id, newdtsregid from registrations where newdtsregid is not null and newdtsregid!=0");
            pMysql.Message = "Tbl_registrations.dts_registration_id - extraction - START ";

            while (dataReader2.Read())
            {
                pPostgres.Update("update tbl_registrations set dts_registration_id=" + dataReader2["newdtsregid"].ToString() + " where id=" + dataReader2["id"].ToString());
            }

            pPostgres.Message = "Tbl_registrations.dts_registration_id - extraction - FINISH";
        }
        public string CheckIfUnix(object data)
                {
                    if (data.GetType().Name == "DBNull" || data == null || data.ToString() == "")
                    {
                        return "null";
                    }
                    string stringData = data.ToString();
                    if (stringData.IndexOf('/') != -1)
                    {
                        return "'"+data.ToString()+"'";
                    } else {
                        return "'" + FromUnixTime(Convert.ToInt64(data)).ToString().Replace(". ", ".") + "'";
                    }
                }
        public string GetStudioId(string pStudio_name, PostgreSQL_DB pPostgres)
        {
            if (String.IsNullOrEmpty(pStudio_name))
            {
                return "null";
            }
            else
            {
                string studioId =
                    GetId("select id from tbl_studios where name like '" + pStudio_name.Replace("'", "''") + "'",
                        pPostgres);
                if (studioId != null)
                {
                    return studioId;
                }
                else
                {
                    return AddNewStudio(pStudio_name, "null", pPostgres);
                }
            }
        }

        private string GetAddressId(string pAddress, string pCity, string pState, string pZip, string pCountryId,
            PostgreSQL_DB pPostgres)
        {
            NpgsqlDataReader query;

            query = pPostgres.Select("select distinct id " +
                                     "from tbl_addresses where address like '" + pAddress.Replace("'", "''") +
                                     "' and city = '" + pCity.Replace("'", "''") + "' and zip like '" + pZip + "' and country_id=" + NVL(pCountryId) + ";");
            string pom;
            while (query.Read())
            {
                pom = query[0].ToString();
                query.Dispose();
                return "'" + pom + "'";
            }
            query.Dispose();
            if (pAddress != "")
            {
                string pomStateId = GetId("select id from tbl_states where name like '" + pState + "'", pPostgres);
                pPostgres.Insert("insert into tbl_addresses(state_id, address, city, zip) values(" + pomStateId +
                                 ",'" + pAddress.Replace("'", "''") + "','" + pCity.Replace("'", "''") + "','" + pZip + "');");
                string p_address_id = GetId("select max(id) from tbl_addresses", pPostgres);
                return p_address_id;
            }
            else
            {
                return "null";
            }
        }

        private string GetPersonId(string pAddress, string pCity, string pState, string pZip, string pCountryId,
            string pFname, string pLname, string pEmail,
            string pContactType, string pPhone, string pPhone2, string pFax, PostgreSQL_DB pPostgres)
        {
            string AddressId = GetAddressId(pAddress, pCity, pState, pZip, pCountryId, pPostgres);
            string GenderId = "null";
            string PersonType = GetId("select id from tbl_person_types where name like '" + pContactType + "' limit 1;", pPostgres);

            // into person
            string birthdate = "null";
            int Max_person_id = Convert.ToInt32(GetId("select max(id) from tbl_persons;", pPostgres));
            pPostgres.Insert(
                "insert into tbl_persons(id, address_id, gender_id, fname, lname, birthdate, person_type_id) " +
                "values("+ ++Max_person_id +"," + AddressId + ", " + GenderId + ",'" + pFname.Replace("'", "''") + "'," +
                "'" + pLname.Replace("'", "''") + "'," + birthdate + "," + PersonType + ");");


            string phone_1 = pPhone;
            string phone_2 = pPhone2;
            var c = new[] { '(', ')', '-', ' ' };
            // insert into studio_has_contact_type
            if (phone_1 != "")
            {
                pPostgres.Insert("insert into person_contact_info(person_id, contact_type_id, value)" +
                                 "values(" + Max_person_id + ",2,'" + Remove(phone_1, c) + "');");
            }
            if (phone_2 != "")
            {
                pPostgres.Insert("insert into person_contact_info(person_id, contact_type_id, value)" +
                                 "values(" + Max_person_id + ",2,'" + Remove(phone_2, c) + "');");
            }
            if (pEmail != "")
            {
                pPostgres.Insert("insert into person_contact_info(person_id, contact_type_id, value)" +
                                 "values(" + Max_person_id + ",1,'" + pEmail.ToLower().Replace("'", "''") + "');");
            }
            if (pFax != "")
            {
                pPostgres.Insert("insert into person_contact_info(person_id, contact_type_id, value)" +
                                 "values(" + Max_person_id + ",8,'" + Remove(pFax, c) + "');");
            }
            return Max_person_id.ToString();
        }
        private string Get_json_boxsets(string b2009, string b2010, string b0910_combo)
        {
            dynamic boxsets = new JObject();
            boxsets.b2009 = Convert.ToInt32(NVLINT(b2009));
            boxsets.b2010 = Convert.ToInt32(NVLINT(b2010));
            boxsets.combo = Convert.ToInt32(NVLINT(b0910_combo));
            return boxsets.ToString();
        }
        private string Get_json_extra(string dvdpre, string gala, string ff, string ace)
        {
            dynamic extra = new JObject();
            extra.dvdpre = Convert.ToInt32(NVLINT(dvdpre));
            extra.gala = Convert.ToInt32(NVLINT(gala));
            extra.ff = Convert.ToInt32(NVLINT(ff));
            extra.ace = Convert.ToInt32(NVLINT(ace));
            return extra.ToString();
        }
        private string Get_json_edu(string studioco, string nostudioco, string college, string k12, string danceteam, string other, string other_val)
        {
            dynamic edu = new JObject();
            edu.studioco = Convert.ToInt32(TFV(studioco));
            edu.nostudioco = Convert.ToInt32(TFV(nostudioco));
            edu.college = Convert.ToInt32(TFV(college));
            edu.k12 = Convert.ToInt32(TFV(k12));
            edu.danceteam = Convert.ToInt32(TFV(danceteam));
            edu.other = Convert.ToInt32(TFV(other));
            edu.other_val = other_val;
            return edu.ToString();
        }
        private string Get_json_fees(string total, string paid)
        {
            dynamic fees = new JObject();
            fees.total = Math.Round(Convert.ToDecimal(NVLINT(total)), 2);
            fees.paid = Math.Round(Convert.ToDecimal(NVLINT(paid)), 2);
            return "'" + fees.ToString() + "'";
        }
    }
}