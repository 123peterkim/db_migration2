﻿using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_workshop_room: BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            pPostgres.Message = "tbl_workshop_rooms - extraction - START";
            pPostgres.Insert("insert into tbl_workshop_rooms(id, name) values('1','Workshop Room 1')");
            pPostgres.Insert("insert into tbl_workshop_rooms(id, name) values('2','Workshop Room 2')");
            pPostgres.Insert("insert into tbl_workshop_rooms(id, name) values('3','Workshop Room 3')");
            pPostgres.Insert("insert into tbl_workshop_rooms(id, name) values('4','Workshop Room 4')");
            pPostgres.Insert("insert into tbl_workshop_rooms(id, name) values('5','Workshop Room 5')");
            pPostgres.Insert("insert into tbl_workshop_rooms(id, name) values('6','Workshop Room 6')");
            pPostgres.Insert("insert into tbl_workshop_rooms(id, name) values('7','Workshop Room 7')");
            pPostgres.Message = "tbl_workshop_rooms - extraction - FINISH";
        }
    }
}