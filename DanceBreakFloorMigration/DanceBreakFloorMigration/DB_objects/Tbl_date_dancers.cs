﻿using System;
using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using Microsoft.VisualBasic;
using MySql.Data.MySqlClient;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Date_dancers : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select *, cast(workshoplevelid as char) as workshoplevelid2 from tbl_date_dancers;");
            pMysql.Message = "date_dancers - extraction - START ";
            while (dataReader.Read())
            {
                string pFee = dataReader["fee"].ToString();
                string statsreg = "null";
                if (String.IsNullOrEmpty(pFee) || pFee== "yrtrdsdsnvhrxz3syy.n")
                {
                    pFee = "0";
                }
                // -------------------
                if (!String.IsNullOrEmpty(dataReader["profileid"].ToString()))
                {
                    CreateDummyDancer(NVL(dataReader["profileid"].ToString()), pPostgres);
                }
                // -------------------
                if (!String.IsNullOrEmpty(dataReader["workshoplevelid2"].ToString()))
                {
                    CreateDummyWorkshopLevel(NVL(dataReader["workshoplevelid2"].ToString()), pPostgres);
                }
                // -------------------

                if (!String.IsNullOrEmpty(dataReader["statsregid"].ToString()))
                {
                    CreateDummyRegistration(NVL(dataReader["statsregid"].ToString()), pPostgres);

                }
                // -------------------
                if (!String.IsNullOrEmpty(dataReader["studioid"].ToString()))
                {
                    CreateDummyStudio(NVL(dataReader["studioid"].ToString()), pPostgres);
                }
                // -------------------
                if (!String.IsNullOrEmpty(dataReader["waiverid"].ToString()))
                {
                    CreateDummyWaiver(NVL(dataReader["waiverid"].ToString()), NVL(dataReader["profileid"].ToString()), pPostgres);
                }
                string NewWorkshopLevel = GetNewWorkshopLevelId(NVLJSON(dataReader["tourdateid"].ToString()),NVLJSON(dataReader["workshoplevelid2"].ToString()), pPostgres);

                string vip_type_id = GetVipTypeId(dataReader["vip_type"].ToString().Replace("'","''"));

                pPostgres.Insert("insert into date_dancers(id, fee, age, one_day, has_scholarship, custom_fee, waiver, attended_reg, " +
                                 "has_photo, vip, independent, vip_type_id, scholarship_code, attended_reg_both, dancer_id, studio_id, tour_date_id, " +
                                 "registration_id, waiver_id, workshop_level_id) " +
                                 "values("+dataReader["id"]+ ",'" + pFee + "'," + dataReader["age"] + "," + CheckBool(dataReader["one_day"].ToString()) + "," + CheckBool(dataReader["has_scholarship"].ToString()) + "," +
                                 "" + CheckBool(dataReader["custom_fee"].ToString()) + "," + CheckBool(dataReader["waiver"].ToString()) + "," + CheckBool(dataReader["attended_reg"].ToString()) + "," + CheckBool(dataReader["has_photo"].ToString()) + "," + CheckBool(dataReader["vip"].ToString()) + "," +
                                 "" + CheckBool(dataReader["independent"].ToString()) + "," + vip_type_id + "," + NVL(dataReader["scholarship_code"].ToString()) + "," + CheckBool(dataReader["attended_reg_both"].ToString()) + "," + NVL(dataReader["profileid"].ToString()) + "," +
                                 "" + NVL(dataReader["studioid"].ToString()) + "," + NVL(dataReader["tourdateid"].ToString()) + ","+statsreg + ", "+NVL(dataReader["waiverid"].ToString()) + ","+ NewWorkshopLevel + ");");
            }
            pPostgres.Message = "date_dancers - extraction - FINISH";
        }

        public string GetVipTypeId(string vip_type)
        {
            if (vip_type == "JUMP_VIP")
            {
                return "1";
            }
            if (vip_type == "NUVO BreakOut")
            {
                return "2";
            }
            if (vip_type == "24 Seven Non-Stop Dancer")
            {
                return "3";
            }
            return "null";
        }

        public string DancerExists(string dancer_id, PostgreSQL_DB pPostgres)
        {
            string check = GetId("select id from tbl_waivers where id = " + dancer_id+";", pPostgres);
            return check;
        }

        public void CreateDummyWaiver(string pWaiverId, string pDancerId, PostgreSQL_DB pPostgres)
        {
            string check = GetId("select id from tbl_waivers where id = " + pWaiverId + ";", pPostgres);
            if (check=="null")
            {
                pPostgres.Insert("insert into tbl_waivers(id, dancer_id) values("+ pWaiverId + ","+pDancerId+");");
            }
        }
        public new void CreateDummyStudio(string pStudio, PostgreSQL_DB pPostgres)
        {
            string check = GetId("select id from tbl_studios where id = " + pStudio + ";", pPostgres);
            if (check == "null")
            {
                pPostgres.Insert("insert into tbl_studios(id, name) values("+ pStudio + ",'DUMMY DANCE STUDIO');");
            }
        }
        public new void CreateDummyRegistration(string pRegistration, PostgreSQL_DB pPostgres)
        {
            string check = GetId("select id from tbl_registrations where id = " + pRegistration + ";", pPostgres);
            if (check == "null" || check == "")
            {
                pPostgres.Insert("insert into tbl_registrations(id) values(" + pRegistration + ");");
            }
        }
        public string GetRegistration(string pRegistration, PostgreSQL_DB pPostgres)
                {
                    string check = GetId("select id from tbl_registrations where id = " + pRegistration + ";", pPostgres);
                    if (check == "null" || check == "")
                    {
                        return "null";
                    }
                    return check;
                }
        public new void CreateDummyWorkshopLevel(string pWorkshopLevel, PostgreSQL_DB pPostgres)
        {
            string check = GetId("select id from tbl_workshop_levels where id = " + pWorkshopLevel + ";", pPostgres);
            if (check == "null")
            {
                pPostgres.Insert("insert into tbl_workshop_levels(id, level_id, season_id) values("+ pWorkshopLevel + ",0,0);");
            }
        }
        public new void CreateDummyDancer(string pDancerId, PostgreSQL_DB pPostgres)
        {
            string check = GetId("select id from tbl_dancers where id = " + pDancerId + ";", pPostgres);
            if (check == "null" )
            {
                pPostgres.Insert("insert into tbl_dancers(id, person_id) values("+ pDancerId + ",0);");
            }
        }

        private string GetNewWorkshopLevelId(string pTourDateId, string pOldId,  PostgreSQL_DB pPostgres)
        {
            if (pTourDateId != "null")
            {
                string pSesonId = GetId("select distinct season_id from tbl_tour_dates where id='" + pTourDateId + "';", pPostgres);
                if (pOldId != "null")
                {
                    string pom = GetId("select id from tbl_workshop_levels where season_id='" + pSesonId + "' and old_id='" + pOldId + "';", pPostgres);
                    return pom;
                }
                return "null";
            }
            return "null";

        }
    }
}