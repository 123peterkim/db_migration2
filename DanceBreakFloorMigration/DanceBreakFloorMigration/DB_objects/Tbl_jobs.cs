﻿using System;
using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_jobs : BaseClass, IMigration
    {

        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from tbl_jobs;");
            pMysql.Message = "tbl_jobs - extraction - START";
            while (dataReader.Read())
            {
                pPostgres.Insert("insert into tbl_jobs(id, title, type, description, views, show) " +
                    "values('" + dataReader["id"] + "','" + dataReader["title"] + "','" + dataReader["jobtype"] + "','" + dataReader["description"].ToString().Replace("'", "''") + "','" + dataReader["views"] + "','" + CheckBool(dataReader["showonsite"].ToString()) + "')");
            }
            pPostgres.Insert("cluster tbl_jobs");
            pPostgres.Message = "tbl_jobs - extraction - FINISH";
        }

    }
}
