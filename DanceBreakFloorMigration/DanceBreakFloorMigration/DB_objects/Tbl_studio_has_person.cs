﻿using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;
using Npgsql;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_studio_has_person: BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            pMysql.Message = "studios_persons - extraction - START";
                pPostgres.Insert("insert into studios_persons(studio_id, person_id) select c.studio_id, a.id from tbl_persons a join tbl_mybtf_users b on (a.id=b.person_id) " +
                                 "join studio_contact_info c on(b.email = c.value) where a.person_type_id in('1','2','3','4','5','6') and c.contact_type_id = 1;");
            pPostgres.Message = "studios_persons - extraction - FINISH";
        }
    }
}   