﻿using System;
using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_store_products_inventory : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from store_products_inventory;");
            pMysql.Message = "tbl_store_products_inventory - extraction - START";

            pPostgres.Insert("insert into tbl_store_colors(id, name) values(0, 'DUMMY');");
            pPostgres.Insert("insert into tbl_store_sizes(id, size) values(0, 'DUMMY');");

            while (dataReader.Read())
            {
                pPostgres.Insert("insert into tbl_store_products_inventory(id, store_product_id, store_size_id, store_color_id, quantity) " +
                                 "values('"+dataReader["id"]+ "','" + dataReader["productid"] + "','" + dataReader["sizeid"] + "','" + dataReader["colorid"] + "'," +
                                 "'" + Get_json_quantity(dataReader["qty_onsite"].ToString(), dataReader["qty_warehouse"].ToString()) + "')");
            }
            pPostgres.Message = "tbl_store_products_inventory - extraction - FINISH";
        }
        private string Get_json_quantity(string on_site, string warehouse)
        {
            dynamic quantity = new JObject();
            quantity.on_site = Convert.ToInt32(NVLINT(on_site));
            quantity.warehouse = Convert.ToInt32(NVLINT(warehouse));
            return quantity.ToString();
        }
    }
}