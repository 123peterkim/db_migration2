using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_event_cities: BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            pMysql.Message = "tbl_event_cities - extraction - START - SKIPPING";

            pPostgres.Message = "tbl_event_cities - extraction - FINISH";
        }


    }
}