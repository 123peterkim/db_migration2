﻿using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;
using Npgsql;
using System;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_routine_categories : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select id, cast(name as char) name, min_dancers, full_fee_per_dancer, duration, cast(abbreviation as char) abbreviation, discount_fee_per_dancer, " +
                                                       "finale_fee_per_dancer, finale_prelim_fee_per_dancer from tbl_routine_categories_11;");

            pMysql.Message = "tbl_routine_categories - extraction - START";
            int counter = 0;
            while (dataReader.Read())
            {
                string categId = GetId("select id from tbl_categories where lower(name) like lower('" + dataReader["name"]+ "');", pPostgres);
                pPostgres.Insert("insert into tbl_routine_categories(id, old_id, season_id, category_id, min_dancers, fee_per_dancer, duration, abbreviation) " +
                                  "values('" + ++counter + "','"+ dataReader["id"].ToString() + "','11','" + categId + "','" + dataReader["min_dancers"] + "','" + Get_json_fee_per_dancer(dataReader["full_fee_per_dancer"].ToString(), dataReader["discount_fee_per_dancer"].ToString(), dataReader["finale_fee_per_dancer"].ToString(), dataReader["finale_prelim_fee_per_dancer"].ToString()) + "','" + dataReader["duration"] + "'," + "'" + dataReader["abbreviation"]+"')");
            }
            dataReader = pMysql.Select("select id, cast(name as char) name, min_dancers, full_fee_per_dancer, duration, cast(abbreviation as char) abbreviation, discount_fee_per_dancer, " +
                                                       "finale_fee_per_dancer, finale_prelim_fee_per_dancer from tbl_routine_categories_14;");
            while (dataReader.Read())
            {
                string categId = GetId("select id from tbl_categories where lower(name) like lower('" + dataReader["name"] + "');", pPostgres);
                pPostgres.Insert("insert into tbl_routine_categories(id, old_id, season_id, category_id, min_dancers, fee_per_dancer, duration, abbreviation) " +
                                  "values('" + ++counter + "','"+ dataReader["id"].ToString() + "','14','" + categId + "','" + dataReader["min_dancers"] + "','" + Get_json_fee_per_dancer(dataReader["full_fee_per_dancer"].ToString(), dataReader["discount_fee_per_dancer"].ToString(), dataReader["finale_fee_per_dancer"].ToString(), dataReader["finale_prelim_fee_per_dancer"].ToString()) + "','" + dataReader["duration"] + "'," + "'" + dataReader["abbreviation"]+"')");
            }
            dataReader = pMysql.Select("select id, cast(name as char) name, min_dancers, full_fee_per_dancer, duration, cast(abbreviation as char) abbreviation, discount_fee_per_dancer, " +
                                                       "finale_fee_per_dancer, finale_prelim_fee_per_dancer from tbl_routine_categories_17;");
            while (dataReader.Read())
            {
                string categId = GetId("select id from tbl_categories where lower(name) like lower('" + dataReader["name"] + "');", pPostgres);
                pPostgres.Insert("insert into tbl_routine_categories(id, old_id, season_id, category_id, min_dancers, fee_per_dancer, duration, abbreviation) " +
                                  "values('" + ++counter + "','"+ dataReader["id"].ToString() + "','17','" + categId + "','" + dataReader["min_dancers"] + "','" + Get_json_fee_per_dancer(dataReader["full_fee_per_dancer"].ToString(), dataReader["discount_fee_per_dancer"].ToString(), dataReader["finale_fee_per_dancer"].ToString(), dataReader["finale_prelim_fee_per_dancer"].ToString()) + "','" + dataReader["duration"] + "'," + "'" + dataReader["abbreviation"]+"')");
            }
            dataReader = pMysql.Select("select id, cast(name as char) name, min_dancers, full_fee_per_dancer, duration, cast(abbreviation as char) abbreviation, discount_fee_per_dancer, " +
                                                       "finale_fee_per_dancer, finale_prelim_fee_per_dancer from tbl_routine_categories_2;");
            while (dataReader.Read())
            {
                string categId = GetId("select id from tbl_categories where lower(name) like lower('" + dataReader["name"] + "');", pPostgres);
                pPostgres.Insert("insert into tbl_routine_categories(id, old_id, season_id, category_id, min_dancers, fee_per_dancer, duration, abbreviation) " +
                                  "values('" + ++counter + "','"+ dataReader["id"].ToString() + "','2','" + categId + "','" + dataReader["min_dancers"] + "','" + Get_json_fee_per_dancer(dataReader["full_fee_per_dancer"].ToString(), dataReader["discount_fee_per_dancer"].ToString(), dataReader["finale_fee_per_dancer"].ToString(), dataReader["finale_prelim_fee_per_dancer"].ToString()) + "','" + dataReader["duration"] + "'," + "'" + dataReader["abbreviation"]+"')");
            }
            dataReader = pMysql.Select("select id, cast(name as char) name, min_dancers, full_fee_per_dancer, duration, cast(abbreviation as char) abbreviation, discount_fee_per_dancer, " +
                                                       "finale_fee_per_dancer, finale_prelim_fee_per_dancer from tbl_routine_categories_20;");
            while (dataReader.Read())
            {
                string categId = GetId("select id from tbl_categories where lower(name) like lower('" + dataReader["name"] + "');", pPostgres);
                pPostgres.Insert("insert into tbl_routine_categories(id, old_id, season_id, category_id, min_dancers, fee_per_dancer, duration, abbreviation) " +
                                  "values('" + ++counter + "','"+ dataReader["id"].ToString() + "','20','" + categId + "','" + dataReader["min_dancers"] + "','" + Get_json_fee_per_dancer(dataReader["full_fee_per_dancer"].ToString(), dataReader["discount_fee_per_dancer"].ToString(), dataReader["finale_fee_per_dancer"].ToString(), dataReader["finale_prelim_fee_per_dancer"].ToString()) + "','" + dataReader["duration"] + "'," + "'" + dataReader["abbreviation"]+"')");
            }
            dataReader = pMysql.Select("select id, cast(name as char) name, min_dancers, full_fee_per_dancer, duration, cast(abbreviation as char) abbreviation, discount_fee_per_dancer, " +
                                                       "finale_fee_per_dancer, finale_prelim_fee_per_dancer from tbl_routine_categories_22;");
            while (dataReader.Read())
            {
                string categId = GetId("select id from tbl_categories where lower(name) like lower('" + dataReader["name"] + "');", pPostgres);
                pPostgres.Insert("insert into tbl_routine_categories(id, old_id, season_id, category_id, min_dancers, fee_per_dancer, duration, abbreviation) " +
                                 "values('" + ++counter + "','"+ dataReader["id"].ToString() + "','22','" + categId + "','" + dataReader["min_dancers"] + "','" + Get_json_fee_per_dancer(dataReader["full_fee_per_dancer"].ToString(), dataReader["discount_fee_per_dancer"].ToString(), dataReader["finale_fee_per_dancer"].ToString(), dataReader["finale_prelim_fee_per_dancer"].ToString()) + "','" + dataReader["duration"] + "'," + "'" + dataReader["abbreviation"]+"')");
            }

            dataReader = pMysql.Select("select id, cast(name as char) name, min_dancers, full_fee_per_dancer, duration, cast(abbreviation as char) abbreviation, discount_fee_per_dancer, " +
                                                      "finale_fee_per_dancer, finale_prelim_fee_per_dancer from tbl_routine_categories_25;");
            while (dataReader.Read())
            {
                string categId = GetId("select id from tbl_categories where lower(name) like lower('" + dataReader["name"] + "');", pPostgres);
                pPostgres.Insert("insert into tbl_routine_categories(id, old_id, season_id, category_id, min_dancers, fee_per_dancer, duration, abbreviation) " +
                                 "values('" + ++counter + "','" + dataReader["id"].ToString() + "','25','" + categId + "','" + dataReader["min_dancers"] + "','" + Get_json_fee_per_dancer(dataReader["full_fee_per_dancer"].ToString(), dataReader["discount_fee_per_dancer"].ToString(), dataReader["finale_fee_per_dancer"].ToString(), dataReader["finale_prelim_fee_per_dancer"].ToString()) + "','" + dataReader["duration"] + "'," + "'" + dataReader["abbreviation"] + "')");
            }

            /*dataReader = pMysql.Select("select id, cast(name as char) name, min_dancers, full_fee_per_dancer, duration, cast(abbreviation as char) abbreviation, discount_fee_per_dancer, " +
                                                      "finale_fee_per_dancer, finale_prelim_fee_per_dancer from tbl_routine_categories_28;");
            while (dataReader.Read())
            {
                string categId = GetId("select id from tbl_categories where lower(name) like lower('" + dataReader["name"] + "');", pPostgres);
                pPostgres.Insert("insert into tbl_routine_categories(id, old_id, season_id, category_id, min_dancers, fee_per_dancer, duration, abbreviation) " +
                                 "values('" + ++counter + "','" + dataReader["id"].ToString() + "','28','" + categId + "','" + dataReader["min_dancers"] + "','" + Get_json_fee_per_dancer(dataReader["full_fee_per_dancer"].ToString(), dataReader["discount_fee_per_dancer"].ToString(), dataReader["finale_fee_per_dancer"].ToString(), dataReader["finale_prelim_fee_per_dancer"].ToString()) + "','" + dataReader["duration"] + "'," + "'" + dataReader["abbreviation"] + "')");
            }
            */
            pPostgres.Insert("insert into tbl_routine_categories(id, category_id, season_id) values(0,1,1)");

            pPostgres.Message = "tbl_routine_categories - extraction - FINISH";
        }

        private string Get_json_fee_per_dancer(string full, string discount, string finale, string finale_prelim)
        {
            dynamic fee_per_dancer = new JObject();
            fee_per_dancer.full = Math.Round(Convert.ToDecimal(NVLINT(full)), 2);
            fee_per_dancer.discount = Math.Round(Convert.ToDecimal(NVLINT(discount)), 2);
            fee_per_dancer.finale = Math.Round(Convert.ToDecimal(NVLINT(finale)), 2);
            fee_per_dancer.finale_prelim = Math.Round(Convert.ToDecimal(NVLINT(finale_prelim)), 2);
            return fee_per_dancer.ToString();
        }
    }
}