﻿using System;
using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_online_critiques : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from tbl_online_critiques_judges;");
            pMysql.Message = "tbl_online_critiques - extraction - START";
            while (dataReader.Read())
            {
                    pPostgres.Insert(
                        "insert into tbl_online_critiques(id, tour_date_id, number, judge, comp_group) " +
                        "values('" + dataReader["id"] + "','" + dataReader["tourdateid"] + "','" + Get_json_number(dataReader["startnumber"].ToString(), CheckBool(dataReader["startnumberhasa"].ToString()), dataReader["endnumber"].ToString(), CheckBool(dataReader["endnumberhasa"].ToString())) + "" +
                        "','"+Get_json_judge(dataReader["judge1"].ToString(), dataReader["judge2"].ToString(), dataReader["judge3"].ToString(), dataReader["judge4"].ToString()).Replace("[", "{").Replace("]","}")  +"'," +
                        "'"+dataReader["compgroup"] +"');");
            }
            pPostgres.Message = "tbl_online_critiques - extraction - FINISH";
        }
        private string Get_json_judge(string pjudge1, string pjudge2, string pjudge3, string pjudge4)
        {
            dynamic judges = new JArray();
            judges.Add(pjudge1);
            judges.Add(pjudge2);
            judges.Add(pjudge3);
            judges.Add(pjudge4);
            return judges.ToString();
        }

        private string Get_json_number(string startnumber, int startnumberhasa, string endnumber, int endnumberhasa)
        {
            dynamic number = new JObject();
            number.start = Convert.ToInt32(NVLINT(startnumber));
            number.start_has_a = Convert.ToInt32(NVLINT(startnumberhasa.ToString()));
            number.end = Convert.ToInt32(NVLINT(endnumber));
            number.end_has_a = Convert.ToInt32(NVLINT(endnumberhasa.ToString()));

            return number.ToString();
        }
    }
}