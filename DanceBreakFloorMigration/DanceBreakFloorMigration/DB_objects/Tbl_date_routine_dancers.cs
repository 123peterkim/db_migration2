﻿using System;
using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Date_routines_dancers: BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select routineid, profileid, tourdateid from tbl_date_routine_dancers group by routineid, profileid, tourdateid;");
            pMysql.Message = "date_routines_dancers - extraction - START";
            while (dataReader.Read())
            {
                string date_routine_id = GetId("select id from date_routines where routine_id="+dataReader["routineid"]+" and tour_date_id="+dataReader["tourdateid"], pPostgres);
                pPostgres.Insert("insert into date_routines_dancers(date_routine_id, dancer_id) values(" + date_routine_id + "," + dataReader["profileid"] + ");");
            }
            pPostgres.Message = "date_routines_dancers - extraction - FINISH";
        }
    }
}