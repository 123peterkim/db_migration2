﻿using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Staff_playlists : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select id, facultyid1 as faculty " +
                                                       "from tbl_date_playlists where facultyid1!='0' and facultyid1 is not null " +
                                                       "union " +
                                                       "select id, facultyid2 as faculty " +
                                                       "from tbl_date_playlists  where facultyid2!='0' and facultyid2 is not null;");
            pMysql.Message = "staff_playlists - extraction - START";
            while (dataReader.Read())
            {
                pPostgres.Insert("insert into staff_playlists(date_playlist_id, staff_id) " +
                                 "values('"+dataReader["id"]+"','"+dataReader["faculty"] +"')");
            }
            pPostgres.Message = "staff_playlists - extraction - FINISH";
        }
    }
}