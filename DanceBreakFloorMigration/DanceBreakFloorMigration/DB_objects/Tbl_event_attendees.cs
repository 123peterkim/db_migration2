﻿using System;
using System.Text;
using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_event_attendees : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from tbl_event_attendees;");
            pMysql.Message = "tbl_event_attendees - extraction - START ";
            while (dataReader.Read())
            {
                /*GET person ID*/
                
                string birthdate;
                if (dataReader["birth_date"].ToString().Equals(""))
                {
                    birthdate = "null";
                } else
                {
                    byte[] birthdateString = (byte[])dataReader["birth_date"];
                    birthdate = FromDashDateToSlash(Encoding.UTF8.GetString(birthdateString));
                }
                 
                string pPersonId = AddNewPerson(dataReader["fname"].ToString(), dataReader["lname"].ToString(), dataReader["email"].ToString(), dataReader["title"].ToString(), birthdate, pPostgres);

                /*Get registrationId*/
                string NewIdFromReg = GetId("select id from tbl_event_registrations where id='" + dataReader["registrationid"]+"'", pPostgres);

                /*Get FEE - in correct format*/
                decimal ParamFee = GetFee(dataReader["fee"].ToString());

                /*Get New RegTypeId*/
                int NewRegType = GetNewRegTypeId(dataReader["tourdateid"].ToString(), dataReader["regtypeid"].ToString(), pPostgres);

                /*insert into target table*/
                pPostgres.Insert("insert into tbl_event_attendees(id,non_commuter,note,canceled,audition_number," +
                                 "promo_code_id,fee,person_id,custom_fee,event_registration_id,event_reg_type_id,registration_id,tour_date_id,studio_id,intensive_id, scholarships) " +
                                 "values(" + dataReader["id"] +","+NVL(dataReader["noncommuterid"].ToString()) + ",'"+ dataReader["note"].ToString().Replace("'", "''") + "'," +
                                 ""+CheckBool(dataReader["canceled"].ToString()) + ","+NVL(dataReader["audition_number"].ToString()) + ", " +
                                 ""+ NVL(dataReader["promocodeid"].ToString()) + ","+ ParamFee + ","+pPersonId+", "+CheckBool(dataReader["custom_fee"].ToString()) + ", "+ NewIdFromReg + ", " +
                                 ""+ NewRegType + ", "+NVLZERO(dataReader["onlineregid"].ToString()) + ","+NVL(dataReader["tourdateid"].ToString()) + "," +
                                 ""+NVL(dataReader["studioid"].ToString()) + ", "+NVL(dataReader["intensiveid"].ToString()) + ",'"+Get_json_cholarships(dataReader["sfrom"].ToString(), dataReader["samt"].ToString()) +"');");
            }
            pPostgres.Message = "tbl_event_attendees - extraction - FINISH";
        }
        public string AddNewPerson(string pName, string pLname, string pEmail, string pTitle, string pBirthDate, PostgreSQL_DB pPostgres)
        {
            string PersonType = GetId("select id from tbl_person_types where name like '" + pTitle + "' limit 1;", pPostgres);
            int MaxPersonId = Convert.ToInt32(GetId("select max(id) from tbl_persons;", pPostgres));
            pPostgres.Insert("insert into tbl_persons(id, fname, lname, person_type_id, birthdate) " +
                          "values("+ ++MaxPersonId +",'" + pName.ToString().Replace("'", "''") + "','" + pLname.ToString().Replace("'", "''") + "'," + PersonType + ","+ pBirthDate +");");
            if (!String.IsNullOrEmpty(pEmail.ToString()))
            {
                pPostgres.Insert("insert into person_contact_info(person_id, contact_type_id, value)" +
                                 "values(" + MaxPersonId + ",1,'" + pEmail.Replace("'", "''") + "');");
            }
            return MaxPersonId.ToString();
        }
        /*
         * Method for getting FEE from origin table
         * Fee in origin table is VARCHAR and in some cases it is empty string
         */
        private decimal GetFee(string pFee)
        {
            if (String.IsNullOrEmpty(pFee))
            {
                return 0;
            }
            return Math.Round(Convert.ToDecimal(NVLINT(pFee.ToString())),2);
        }
        /*
         * Method which return: scholarships JSON
         */
        private string Get_json_cholarships(string pSfrom, string pSamt)
        {
            dynamic scholarships = new JObject();
            if (pSamt == "half" || pSamt == "full")
            {
                scholarships.amount = pSamt;
            }
            else
            {
                scholarships.amount = Math.Round(Convert.ToDecimal(NVLINT(pSamt)), 2);
            }
            scholarships.from = pSfrom.Replace("'","''");
            return scholarships.ToString();
        }
        /*
         * method which return new value of REG_TYPE
         * !!! - in some cases the old_id=0 and this value is incorect, so I decided to return 1 for this case
         * Peter - can you check this situation? For example when tbl_event_attendees.tourdateid=441
         */
        private int GetNewRegTypeId(string pTourDateId, string pOldId,  PostgreSQL_DB pPostgres)
        {
            string pSesonId= GetId("select distinct season_id from tbl_tour_dates where id='"+pTourDateId+"';", pPostgres);
            string pom = GetId("select id from tbl_event_reg_types where season_id='"+pSesonId+"' and old_id='"+pOldId+"';", pPostgres);
            if (pom=="null")
            {
                return 1;
            }
            return Convert.ToInt32(pom);

        }
    }
}