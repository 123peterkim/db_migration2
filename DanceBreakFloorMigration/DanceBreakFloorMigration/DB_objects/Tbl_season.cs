﻿using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_season: BaseClass, IMigration
    {
        public void SupRemigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
                {            
                    MySqlDataReader dataReader = pMysql.Select("select id, start_year, end_year, type from tbl_seasons;");
        
                    pMysql.Message = "tbl_seasons - extraction - START";
                    string pom1 = "null";
                    string pom2 = "null";
                    while (dataReader.Read())
                    {
                        // update part start HERE
                        if (!String.IsNullOrEmpty(dataReader["update_date"].ToString()) && Convert.ToDateTime(dataReader["update_date"].ToString()) >= Convert.ToDateTime(pDate))
                        {                    
                            List<string> MySQLData = new List<string> {"null", dataReader["id"].ToString(), dataReader["start_year"].ToString(), dataReader["end_year"].ToString()};
                            List<string> PostgreSQLData = new List<string> {"tbl_seasons", "id", "start_year", "end_year"};
                            UpdatePostgresRow(dataReader["id"].ToString(), MySQLData, PostgreSQLData,  pPostgres);
                        }
                        
                        if (GetId("select id from tbl_seasons where id = " + dataReader["id"] + ";", pPostgres) == "null" && !String.IsNullOrEmpty(dataReader["insert_date"].ToString()) && Convert.ToDateTime(dataReader["insert_date"].ToString()) >= Convert.ToDateTime(pDate))
                                                {
                                                    pom1 = (dataReader[2].ToString()=="")?"null": dataReader[2].ToString();
                                                    pPostgres.Insert("insert into tbl_seasons(id, start_year,end_year) values('" + dataReader[0] + "',"+ dataReader[1] + ","+ pom1 + ")");
                                                }
                        // update part END
                    }
        
                    pPostgres.Message = "tbl_seasons - extraction - FINISH";
                }
        
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select id, year1, year2, type from tbl_seasons;");
            pMysql.Message = "tbl_seasons - extraction - START";
            string pom = "null";
            while (dataReader.Read())
            {
                pom = (dataReader[2].ToString()=="")?"null": dataReader[2].ToString();
                pPostgres.Insert("insert into tbl_seasons(id, start_year,end_year, event_type_id) values('" + dataReader[0] + "',"+ dataReader[1] + ","+ pom + ","+ dataReader[3].ToString() +")");
            }
            pPostgres.Insert("insert into tbl_seasons(id, start_year, end_year) values(0, 9999,9999)");
            pPostgres.Message = "tbl_seasons - extraction - FINISH";
        }
    }
}