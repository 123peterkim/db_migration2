﻿using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;
using System;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_admins : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from users;");
            pMysql.Message = "tbl_admins - extraction - START ";
            while (dataReader.Read())
            {
                pPostgres.Insert("insert into tbl_admins(id, password, last_login, ip, name, theme, financials, access_level) " +
                                 "values('"+dataReader["id"]+"','"+dataReader["password"]+"',"+ CheckIfExists(dataReader["last_login"])+",'"+dataReader["ip"]+"'," +
                                 "'"+dataReader["name"]+"','"+dataReader["theme"]+ "','" + CheckBool(dataReader["financials"].ToString()) + "','" +CheckBool(dataReader["accesslevel"].ToString()) + "');");
            }
            pPostgres.Insert("insert into tbl_admins(id, name) values(21, 'DUMMY')");
            pPostgres.Insert("insert into tbl_admins(id, name) values(0, 'DUMMY')");
            pPostgres.Insert("insert into tbl_admins(id, name) values(34, 'DUMMY')");
            pPostgres.Insert("insert into tbl_admins(id, name) values(24, 'DUMMY')");
            pPostgres.Insert("insert into tbl_admins(id, name) values(38, 'DUMMY')");
            pPostgres.Insert("insert into tbl_admins(id, name) values(46, 'DUMMY')");
            pPostgres.Insert("insert into tbl_admins(id, name) values(32, 'DUMMY')");
            pPostgres.Insert("insert into tbl_admins(id, name) values(33, 'DUMMY')");

            pPostgres.Insert("insert into tbl_admins(id, name) values(12, 'DUMMY')");
            pPostgres.Insert("insert into tbl_admins(id, name) values(23, 'DUMMY')");
            pPostgres.Message = "tbl_admins - extraction - FINISH";
        }

        private string CheckIfExists(object data)
        {
            if (data.GetType().Name == "DBNull" || data == null)
            {
                return "null";
            }
            else
            {
                return "'" + FromUnixTime(Convert.ToInt64(data)).ToString().Replace(". ", ".") + "'";
            }
        }
    }
}