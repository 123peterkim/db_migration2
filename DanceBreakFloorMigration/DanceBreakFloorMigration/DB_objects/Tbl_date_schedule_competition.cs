﻿using System;
using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Date_schedule_competitions : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from tbl_date_schedule_competition;");
            pMysql.Message = "date_schedule_competitions - extraction - START";
            while (dataReader.Read())
            {
                    pPostgres.Insert("insert into date_schedule_competitions(id, tour_date_id,last_update,dates,awards) " +
                                 "values('" + dataReader["id"] + "'," +
                                 "'" + dataReader["tourdateid"] + "'," + NVL(dataReader["last_update"].ToString().Replace("'","''")) + "," + NVL(dataReader["dates"].ToString().Replace("'", "''")) + "," + NVL(dataReader["awards"].ToString().Replace("'", "''")) + ")");
            }
            pPostgres.Message = "date_schedule_competitions - extraction - FINISH";
        }
    }
}