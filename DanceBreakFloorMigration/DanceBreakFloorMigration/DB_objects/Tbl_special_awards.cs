﻿using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_special_awards : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from tbl_special_awards");

            pMysql.Message = "Tbl_special_awards - extraction - START";
            int pcounter = 0;
            while (dataReader.Read())
            {
                string p_award_name = GetId("select id from tbl_award_types where name like '" + dataReader["name"].ToString().Replace("'", "''") + "' limit 1", pPostgres);
                string age_division_id = "null";
                string performance_division_id = "null";
                if (p_award_name == "1" || p_award_name == "6" || p_award_name == "11" || p_award_name == "35")
                {
                     age_division_id = "4";
                }
                if (p_award_name == "2" || p_award_name == "7" || p_award_name == "12" || p_award_name == "34")
                {
                     age_division_id = "3";
                }
                if (p_award_name == "3" || p_award_name == "8" || p_award_name == "13" || p_award_name == "33")
                {
                     age_division_id = "2";
                }
                if (p_award_name == "4" || p_award_name == "9" || p_award_name == "14" || p_award_name == "32")
                {
                     age_division_id = "1";
                }
                if (p_award_name == "37" || p_award_name == "39" || p_award_name == "38")
                {
                     age_division_id = "8";
                }
                if (p_award_name == "40")
                {
                     age_division_id = "5";
                }
                if (p_award_name == "41")
                {
                     age_division_id = "6";
                }
                if (p_award_name == "42")
                {
                     age_division_id = "10";
                }
                if (p_award_name == "43")
                {
                     age_division_id = "7";
                }
                if (p_award_name == "20")
                {
                    performance_division_id = "3";
                }
                if (p_award_name == "21")
                {
                    performance_division_id = "1";
                }
                if (p_award_name == "22")
                {
                    performance_division_id = "2";
                }
                if (p_award_name == "23")
                {
                    performance_division_id = "9";
                }
                if (p_award_name == "24")
                {
                    performance_division_id = "8";
                }
                if (p_award_name == "25")
                {
                    performance_division_id = "5";
                }
                if (p_award_name == "26")
                {
                    performance_division_id = "4";
                }
                if (p_award_name == "30")
                {
                    performance_division_id = "7";
                }
                pPostgres.Insert("insert into tbl_special_awards(id, event_id, award_type_id, report_order, age_division_id, performance_division_id) " +
                                 "values('" + dataReader["id"] + "','" + dataReader["eventid"] + "','" + p_award_name + "','"+dataReader["report_order"] +"',"+age_division_id+","+performance_division_id+")");
            }
            pPostgres.Message = "Tbl_special_awards - extraction - FINISH";
        }
    }
}