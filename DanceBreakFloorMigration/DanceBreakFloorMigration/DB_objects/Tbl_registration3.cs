using System;
using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Dynamic;
using System.Web.Script.Serialization;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json;
using Npgsql;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_registrations3 : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            // tbl_registrations from dancetea.registration
            // ----------------------------------------------------------
            MySqlDataReader dataReader = pMysql.Select("select * from tbl_user_registrations where status=1 and statsregid is not null and statsregid != 0");

            pMysql.Message = "tbl_registrations (mybreak.tbl_user_registrations.status=1) - extraction - START ";
            while (dataReader.Read())
            {
                dynamic data;
                try {
                                                   data = JsonConvert.DeserializeObject<dynamic>(dataReader["regdata"].ToString());
                                                } catch (JsonReaderException)
                                                {
                    data = "null";
                }
                string creditamt = "";
                string creditfrom = "";
                try
                {
                    creditamt = data["Payment"]["creditamt"].ToString();
                } catch (Exception ex)
                                                      {
                                                          if (ex is RuntimeBinderException || ex is InvalidOperationException)
                                                          {
                    creditamt = "null";
                    }
                }
                try
                {
                    creditfrom = data["Payment"]["creditfrom"].ToString();
                } catch (Exception ex)
                                                      {
                                                          if (ex is RuntimeBinderException || ex is InvalidOperationException)
                                                          {
                    creditfrom= "null";}
                }
                string address = "";
                string city = "";
                string state = "";
                string zip = "";
                string country = "";
                try
                {
                    address = data["Contact"]["address"].ToString();
                } catch (Exception ex)
                                                      {
                                                          if (ex is RuntimeBinderException || ex is InvalidOperationException)
                                                          {
                    address = "null";}
                }
                try
                {
                    city = data["Contact"]["city"].ToString();
                } catch (Exception ex)
                                                      {
                                                          if (ex is RuntimeBinderException || ex is InvalidOperationException)
                                                          {
                    city= "null";}
                }
                try
                {
                    state = data["Contact"]["state"].ToString();
                } catch (Exception ex)
                                                      {
                                                          if (ex is RuntimeBinderException || ex is InvalidOperationException)
                                                          {
                    state= "null";}
                }
                try
                {
                    zip = data["Contact"]["zip"].ToString();
                } catch (Exception ex)
                                                      {
                                                          if (ex is RuntimeBinderException || ex is InvalidOperationException)
                                                          {
                    zip= "null";}
                }
                try
                {
                    country = data["Contact"]["countryid"].ToString();
                } catch (Exception ex)
                                                      {
                                                          if (ex is RuntimeBinderException || ex is InvalidOperationException)
                                                          {
                    country= "null";}
                }

                string person_type_id = "";
                try
                {
                    person_type_id = data["Contact"]["typeid"].ToString();
                    if (person_type_id == "1")
                    {
                        person_type_id = "3";
                    }
                    if (person_type_id == "2")
                    {
                        person_type_id = "5";
                    }
                    if (person_type_id == "3")
                    {
                        person_type_id = "8";
                    }
                    if (person_type_id == "4")
                    {
                        person_type_id = "9";
                    }
                    if (person_type_id == "5")
                    {
                        person_type_id = "7";
                    }
                    if (person_type_id == "6")
                    {
                        person_type_id = "2";
                    }
                } catch (Exception ex)
                                                      {
                                                          if (ex is RuntimeBinderException || ex is InvalidOperationException)
                                                          {
                    person_type_id= "null";}
                }

                string email = "";
                try
                {
                    email = data["Contact"]["email"].ToString();
                } catch (Exception ex)
                                                      {
                                                          if (ex is RuntimeBinderException || ex is InvalidOperationException)
                                                          {
                    email= "null";}
                }

                string phone = "";
                try
                {
                    phone = data["Contact"]["phone"].ToString();
                } catch (Exception ex)
                                                      {
                                                          if (ex is RuntimeBinderException || ex is InvalidOperationException)
                                                          {
                    phone= "null";}
                }

                string phone2 = "";
                try
                {
                    phone2 = data["Contact"]["phone2"].ToString();
                } catch (Exception ex)
                                                      {
                                                          if (ex is RuntimeBinderException || ex is InvalidOperationException)
                                                          {
                    phone2= "null";}
                }

                string fax = "";
                try
                {
                    fax = data["Contact"]["fax"].ToString();
                } catch (Exception ex)
                                                      {
                                                          if (ex is RuntimeBinderException || ex is InvalidOperationException)
                                                          {
                    fax= "null";}
                }

                string independent = "";
                if (dataReader["independent"].ToString() != "")
                {
                    independent = dataReader["independent"].ToString();
                } else {
                     independent = "null";
                 }
                try
                {
                    independent = data["Contact"]["independent"].ToString();
                } catch (Exception ex)
                                                      {
                                                          if (ex is RuntimeBinderException || ex is InvalidOperationException)
                                                          {
                    independent= "null";}
                }
                string dancer_id = "";
                try
                {
                    dancer_id = data["Contact"]["regprofileid"].ToString();
                } catch (Exception ex)
                                                      {
                                                          if (ex is RuntimeBinderException || ex is InvalidOperationException)
                                                          {
                    dancer_id= "null";}
                }
                string payment_method = "";
                try
                {
                    payment_method = data["Payment"]["payment_method"].ToString();
                } catch (Exception ex)
                                                      {
                                                          if (ex is RuntimeBinderException || ex is InvalidOperationException)
                                                          {
                    payment_method= "null";}
                }
                string studioname = "";
                try
                {
                    studioname = data["Contact"]["studioname"].ToString();
                    studioname = studioname.Replace("'","''").Replace("\\", "");
                } catch (Exception ex)
                                                      {
                                                          if (ex is RuntimeBinderException || ex is InvalidOperationException)
                                                          {
                 studioname= "null";}
                }
                string studioowner = "";
                try
                {
                    studioowner = data["Contact"]["studioowner"].ToString();
                    studioowner = studioowner.Replace("'","''").Replace("\\", "");
                } catch (Exception ex)
                                                      {
                                                          if (ex is RuntimeBinderException || ex is InvalidOperationException)
                                                          {
                    studioowner= "null";}
                }

                string fname = "";
                try
                {
                    fname = data["Contact"]["fname"].ToString().Replace("'", "''");
                } catch (Exception ex)
                                                      {
                                                          if (ex is RuntimeBinderException || ex is InvalidOperationException)
                                                          {
                    fname= "null";}
                }

                string lname = "";
                try
                {
                    lname = data["Contact"]["lname"].ToString().Replace("'", "''");
                } catch (Exception ex)
                                                      {
                                                          if (ex is RuntimeBinderException || ex is InvalidOperationException)
                                                          {
                    lname= "null";
                    }
                }

                string organization = "";
                try
                {
                    organization = data["Contact"]["organization"].ToString();
                    organization = organization.Replace("'", "''").Replace("\\", "");
                }
                catch (Exception ex)
                {
                    if (ex is RuntimeBinderException || ex is InvalidOperationException)
                    {
                        organization = "null";
                    }
                }


                string completed = GetId("select dates->>'completed' as completed from tbl_registrations where id="+dataReader["statsregid"].ToString(), pPostgres);
                string confirmed = GetId("select dates->>'confirmed' as confirmed from tbl_registrations where id="+dataReader["statsregid"].ToString(), pPostgres);

                pPostgres.Update("update tbl_registrations set "+
                "mybtf_user_id="+ UserIdManage(dataReader["userid"].ToString(), fname, lname, person_type_id, email, phone, phone2, fax, dancer_id, address, city, state, zip, country, pPostgres) +", "+
                "credit='"+Get_json_credit(dataReader["creditamt"].ToString(),dataReader["creditfrom"].ToString(), creditamt, creditfrom) +"', "+
                "free_teacher='"+Get_json_free_teacher(dataReader["ftc"].ToString(), dataReader["ftv"].ToString()) +"', "+
                "left_off="+NVLNULL(dataReader["leftoffphp"].ToString())+", "+
                "observers='"+ Get_json_observers(dataReader["observers"].ToString(),dataReader["observers2"].ToString()) + "', "+
                "dates='"+Get_json_date(CheckIfExists(dataReader["mktime"]),CheckIfExists(dataReader["mkupdate"]), completed, confirmed) +"', "+
                "fees='"+ Get_json_fees(dataReader["workshop_fee"].ToString(), dataReader["competition_fee"].ToString(), dataReader["attendee_fee"].ToString(),dataReader["bestdancer_fee"].ToString(),  dataReader["offers_fee"].ToString(),  dataReader["observers_fee"].ToString(), dataReader["observers_fee2"].ToString(), dataReader["total_fees"].ToString()) +"', "+
                "waiver="+CheckBool(dataReader["waiver"].ToString()) +" where id="+dataReader["statsregid"].ToString());
            }

            pPostgres.Message = "tbl_registrations (mybreak.tbl_user_registrations) - extraction - FINISH";
        }

        private string CheckIfExists(object data)
        {
            if (data.GetType().Name == "DBNull" || data == null)
            {
                return "null";
            }
            else
            {
                return FromUnixTime(Convert.ToInt64(data)).ToString().Replace(". ", ".");
            }
        }

        public string GetPaymentMethodId(string paymentMethod)
                {
                    if (paymentMethod == "credit_card")
                    {
                        return "1";
                    }
                    if (paymentMethod == "check")
                    {
                        return "2";
                    }
                    if (paymentMethod == "nobal")
                    {
                        return "3";
                    }
                    return paymentMethod;
                }

        public string GetStudioId(string studioname, PostgreSQL_DB pPostgres)
                {
                    if (studioname != "" && studioname != "null" && studioname != null)
                    {
                        string studioId = GetId("select id from tbl_studios where name like '" + studioname.Replace("'", "''") + "'", pPostgres);
                        if (studioId != null)
                        {
                            return studioId;
                        }
                        else
                        {
                            return AddNewStudio(studioname, "null", pPostgres);
                        }
                    } else
                    {
                        return "null";
                    }
                }
        private string Get_json_date(string created, string updated, string completed, string confirmed)
        {
            dynamic date = new JObject();
            if (completed == "null")
            {
                date.completed = null;
            }
            else
            {
                date.completed = completed;

            }
            if (confirmed == "null")
            {
                date.confirmed = null;
            }
            else
            {
                date.confirmed = confirmed;

            }
            if (created == "null")
            {
                date.created = null;
            }
            else
            {
                date.created = created;

            }
            if (updated == "null")
            {
                date.updated = null;
            }
            else
            {
                date.updated = updated;

            }
            return date.ToString();
        }
        private string Get_json_fees(string pWorkshop, string pCompetition, string pAttendee, string pBestDancer, string pOffers, string pObser1, string pObser2, string total)
        {
            dynamic fees = new JObject();
            fees.workshop = Math.Round(Convert.ToDecimal(NVLINT(pWorkshop)), 2);
            fees.competition = Math.Round(Convert.ToDecimal(NVLINT(pCompetition)), 2);
            fees.attendee = Math.Round(Convert.ToDecimal(NVLINT(pAttendee)), 2);
            fees.bestdancer = Math.Round(Convert.ToDecimal(NVLINT(pBestDancer)), 2);
            fees.offer = Math.Round(Convert.ToDecimal(NVLINT(pOffers)), 2);
            fees.total = Math.Round(Convert.ToDecimal(NVLINT(total)), 2);

            dynamic observers = new JObject();
            observers.one = Math.Round(Convert.ToDecimal(NVLINT(pObser1)), 2);
            observers.two = Math.Round(Convert.ToDecimal(NVLINT(pObser2)), 2);

            fees.observer = observers;
            return fees.ToString();
        }
        private string Get_json_observers(string pObservers, string pObservers2)
        {
            dynamic observers = new JObject();
            observers.one = Convert.ToInt32(NVLINT(pObservers));
            observers.two = Convert.ToInt32(NVLINT(pObservers2));
            return observers.ToString();
        }
        private string Get_json_free_teacher(string pCount, string pValue)
        {
            dynamic free_teacher = new JObject();
            free_teacher.count = Convert.ToInt32(NVLINT(pCount));
            free_teacher.value = Math.Round(Convert.ToDecimal(NVLINT(pValue)), 2);
            return free_teacher.ToString();
        }
        private string Get_json_credit(string pAmmount, string pFrom, string creditamt, string creditfrom)
        {
            dynamic credit = new JObject();
            credit.amount = pAmmount.Replace("$", "").Replace("usd", "").Replace(",", "").Replace(" USD", "");
            credit.from = pFrom.Replace("'","''");
            if (creditamt != "null")
            {
                credit.amount = creditamt.Replace("$", "").Replace("usd", "").Replace(",", "").Replace(" USD", "");
            }
            if (creditfrom != "null")
            {
                credit.from = creditfrom.Replace("'","''");
            }
            if (credit.from == "")
            {
                credit.from = null;
            }
            if (credit.amount == "" || credit.amount == "null")
            {
                credit.amount = 0;
            }
            return credit.ToString();
        }
        public string Get_address(string address, string city, string state, string zip, string countryid, PostgreSQL_DB pg)
        {
            string existingAddressId = GetId("select id from tbl_addresses where address ilike '"+address.Replace("'", "''")+"' and city ilike '"+city.Replace("'", "''")+"';", pg);
            if (existingAddressId == "null")
            {
                string pomStateId = GetId("select id from tbl_states where name ilike '" + state + "'", pg);
                int maxAddressId = Convert.ToInt32(GetId("select max(id) from tbl_addresses", pg));
                pg.Insert("insert into tbl_addresses(id, address, city, state_id, zip, country_id) " + "values('" + ++maxAddressId + "'," + NVLNULL(address.Replace("'", "''")) + "," + NVLNULL(city.Replace("'", "''")) + "," + NVLNULL(pomStateId) + "," + NVLNULL(zip) + "," + NVLNULL(countryid) + ");");
                existingAddressId = maxAddressId.ToString();
            }
            return existingAddressId;
        }

        public string UserIdManage(string mybtf_user_id, string pname, string lname, string person_type_id, string email, string phone, string phone2, string fax, string dancer_id, string address, string city, string state, string zip, string country, PostgreSQL_DB pPostgres)
        {
            if (mybtf_user_id == "null")
            {
                string address_id = Get_address(address, city, state, zip, country, pPostgres);
                pPostgres.Insert("insert into tbl_persons(address_id, gender_id, fname, lname, birthdate, person_type_id) " +
                                 "values("+address_id+",null,'" + pname + "','" + lname + "',null, " + person_type_id + ") ");
                string Max_person_id = GetId("select max(id) from tbl_persons", pPostgres);

                if (email != "null" && email != "")
                {
                    pPostgres.Insert("insert into person_contact_info(person_id, contact_type_id, value) " + "values('"+Max_person_id+"',1,"+NVLNULL(email)+");");
                }
                if (phone != "null" && phone != "")
                {
                    pPostgres.Insert("insert into person_contact_info(person_id, contact_type_id, value) " + "values('"+Max_person_id+"',2,"+NVLNULL(phone)+");");
                }
                if (phone2 != "null" && phone2 != "")
                {
                    pPostgres.Insert("insert into person_contact_info(person_id, contact_type_id, value) " + "values('"+Max_person_id+"',2,"+NVLNULL(phone2)+");");
                }
                if (fax != "null" && fax != "")
                {
                    pPostgres.Insert("insert into person_contact_info(person_id, contact_type_id, value) " + "values('"+Max_person_id+"',8,"+NVLNULL(fax)+");");
                }

                int Max_mybtf_user_id = Convert.ToInt32(GetId("select max(id) from tbl_mybtf_users", pPostgres));
                pPostgres.Insert("insert into tbl_mybtf_users(id, email, password, active, person_id, unregistered, dancer_id) " +
                                 "values('" + ++Max_mybtf_user_id + "','" + email + "',null,null,'" + Max_person_id + "','1', "+dancer_id+")");

                mybtf_user_id = Max_mybtf_user_id.ToString();
            }
            else
            {
                NpgsqlDataReader query;
                string query_person_id = "";
                string query_address_id = "";
                string query_contact_info = "";
                query = pPostgres.Select("select tbl_mybtf_users.id as mybtf_user_id, tbl_persons.id as person_id, tbl_addresses.id as address_id, (select json_agg(contact_info) from (select * from person_contact_info where person_id=tbl_mybtf_users.person_id) contact_info) as contact_info from tbl_mybtf_users left join tbl_persons on tbl_persons.id=tbl_mybtf_users.person_id left join tbl_addresses on tbl_addresses.id=tbl_persons.address_id where tbl_mybtf_users.id="+mybtf_user_id+" limit 1;");
                while (query.Read())
                {
                    query_person_id = query["person_id"].ToString();
                    query_address_id = query["address_id"].ToString();
                    query_contact_info = query["contact_info"].ToString();
                }
                query.Dispose();


                string person_id = query_person_id;
                string address_id = "";
                if (query_person_id == "" || query_person_id == "null")
                {
                    pPostgres.Insert("insert into tbl_persons(gender_id, fname, lname, birthdate, person_type_id) " +
                                     "values(null,'" + pname + "','" + lname + "',null, " + person_type_id + ") ");
                    person_id = GetId("select max(id) from tbl_persons", pPostgres);
                    pPostgres.Update("update tbl_mybtf_users set person_id=" + person_id + " where id=" + mybtf_user_id + ";");
                }
                if (query_address_id == "" || query_address_id == "null")
                {
                    address_id = Get_address(address, city, state, zip, country, pPostgres);
                    pPostgres.Update("update tbl_persons set address_id=" + address_id + " where id=" + person_id + ";");
                }
                


                if (query_contact_info == "" || query_contact_info == "null")
                {
                    if (email != "null" && email != "")
                    {
                        pPostgres.Insert("insert into person_contact_info(person_id, contact_type_id, value) " + "values('" + person_id + "',1," + NVLNULL(email) + ");");
                    }
                    if (phone != "null" && phone != "")
                    {
                        pPostgres.Insert("insert into person_contact_info(person_id, contact_type_id, value) " + "values('" + person_id + "',2," + NVLNULL(phone) + ");");
                    }
                    if (phone2 != "null" && phone2 != "")
                    {
                        pPostgres.Insert("insert into person_contact_info(person_id, contact_type_id, value) " + "values('" + person_id + "',2," + NVLNULL(phone2) + ");");
                    }
                    if (fax != "null" && fax != "")
                    {
                        pPostgres.Insert("insert into person_contact_info(person_id, contact_type_id, value) " + "values('" + person_id + "',8," + NVLNULL(fax) + ");");
                    }
                }
            }
            return mybtf_user_id;
        }
    }
}