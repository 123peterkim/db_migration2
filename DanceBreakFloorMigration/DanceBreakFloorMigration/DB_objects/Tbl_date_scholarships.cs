﻿using System;
using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Date_scholarships : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from tbl_date_scholarships");
            pMysql.Message = "date_scholarships - extraction - START ";
            while (dataReader.Read())
            {
                // -------------------
                if (!String.IsNullOrEmpty(dataReader["facultyid"].ToString()))
                {
                    CreateDummyFaculty(NVL(dataReader["facultyid"].ToString()), pPostgres);
                }

                // -------------------
                if (!String.IsNullOrEmpty(dataReader["datedancerid"].ToString()))
                {
                    CreateDummyTblDateDancers(NVL(dataReader["datedancerid"].ToString()), pPostgres);
                }

                pPostgres.Insert("insert into date_scholarships(id, tour_date_id, scholarship_id, winner, code, staff_id, dancer_id, date_dancer_id) " +
                                 "values("+dataReader["id"]+","+dataReader["tourdateid"] +","+dataReader["scholarshipid"] +"," +
                                 ""+CheckBool(dataReader["winner"].ToString()) + ","+NVL(dataReader["code"].ToString()) + ","+NVLZERO(dataReader["facultyid"].ToString()) + "," +
                                 ""+NVL(dataReader["profileid"].ToString()) + ","+NVL(dataReader["datedancerid"].ToString()) + ")");
            }
            pPostgres.Message = "date_scholarships - extraction - FINISH";
        }
    }
}