﻿using System;
using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;
using Npgsql;
using Newtonsoft.Json.Linq;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_faculty : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from tbl_faculty;");
            pMysql.Message = "tbl_faculty - extraction - START";

           
            while (dataReader.Read())
            {
                string event_id = GetId("select id from tbl_events where lower(name) like lower('"+dataReader["tour"]+"')",pPostgres);

                string person_id = GetId("select id from tbl_persons where lower(fname) like lower('" + dataReader["fname"].ToString().Replace("'", "''") + "') " +
                                         "and lower(lname) like lower('"+ dataReader["lname"].ToString().Replace("'", "''") + "')", pPostgres);
                
                string style_1 = GetId("select id from tbl_performance_divisions where name like '"+dataReader["style1"] +"'", pPostgres);
                string style_2 = GetId("select id from tbl_performance_divisions where name like '" + dataReader["style2"] + "'", pPostgres);

                if (person_id == "null")
                {
                    pPostgres.Insert("insert into tbl_persons(fname, lname, person_type_id) values('" + dataReader["fname"].ToString().Replace("'", "''") + "','" + dataReader["lname"].ToString().Replace("'", "''") +"','"+ "13" + "')");
                    person_id = GetId("select max(id) from tbl_persons;", pPostgres);
                }
                pPostgres.Insert("insert into tbl_faculty(id, event_id, list_order, person_id, bio, director, links) " +
                                "values(" + dataReader["id"] + "," + event_id + "," + NVL(dataReader["listorder"].ToString()) + "," + person_id + ",'" + dataReader["bio"].ToString().Replace("'", "''") + "','" +
                                dataReader["director"].ToString().Replace("'", "''") + "'," + Get_json_links(dataReader["website"].ToString().Replace("'", "''"), dataReader["twitter"].ToString().Replace("'", "''"), dataReader["instagram"].ToString().Replace("'", "''"), dataReader["youtube"].ToString().Replace("'", "''")) + ");");
                if (style_1 != "null")
                {
                    pPostgres.Insert("insert into faculty_performance_divisions(faculty_id, performance_division_id) " +
                                     "values(" + dataReader["id"] + "," + style_1 + ");");
                }
                if (style_2 != "null")
                {
                    pPostgres.Insert("insert into faculty_performance_divisions(faculty_id, performance_division_id) " +
                                     "values(" + dataReader["id"] + "," + style_2 + ");");
                }
            }
            pPostgres.Message = "tbl_faculty - extraction - FINISH";
        }

        private string Get_json_links(string website, string twitter, string instagram, string youtube)
        {
            dynamic links = new JObject();
            links.website = website;
            links.twitter = twitter;
            links.instagram = instagram;
            links.youtube = youtube;
            return "'" + links.ToString() + "'";
        }
        
    }
}