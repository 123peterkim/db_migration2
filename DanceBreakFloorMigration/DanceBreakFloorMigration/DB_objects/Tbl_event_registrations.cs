﻿using System;
using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;
using Npgsql;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_event_registrations : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from tbl_event_registrations");
            pMysql.Message = "Tbl_event_registrations - extraction - START ";
            while (dataReader.Read())
            {
                pPostgres.Insert("SELECT setval('tbl_addresses_id_seq', (SELECT MAX(id) FROM tbl_addresses))");
                string PersonId = GetPersonId(dataReader["address"].ToString().Replace("'", "''"), dataReader["city"].ToString().Replace("'", "''"),
                    dataReader["state"].ToString().Replace("'", "''"), dataReader["zip"].ToString(),
                    dataReader["countryid"].ToString(), dataReader["fname"].ToString().Replace("'","''"), dataReader["lname"].ToString().Replace("'", "''"),
                    dataReader["email"].ToString(),
                    dataReader["contact_type"].ToString(), dataReader["phone"].ToString(),
                    dataReader["phone2"].ToString(), pPostgres);
                string studioid = GetStudioId(dataReader["studioid"].ToString(), dataReader["organization"].ToString(),
                    pPostgres);
                string notes = (dataReader["notes"].ToString() == "") ? "null" : "'" + dataReader["notes"].ToString().Replace(Environment.NewLine, "").Replace("\r\n", "").Replace("\r", "").Replace("\n", "") + "'";

                pPostgres.Insert(
                    "insert into tbl_event_registrations(id, tour_date_id, studio_id, person_id, registration_id, " +
                    "mybtf_user_id, admin_id, heard, notes, total_fees, fees_paid, balance_due, pay_choice, registration_date) " +
                    "values(" + dataReader["id"] + "," + dataReader["tourdateid"] + ", "+ studioid + "," +
                    ""+PersonId+"," +
                    ""+NVL(dataReader["onlineregid"].ToString()) + ","+NVL(dataReader["onlineuserid"].ToString()) + ", " +
                    ""+NVL(dataReader["enteredbyid"].ToString()) + ", '"+dataReader["heard"].ToString().Replace("'","''") +"',"+ notes + "" +
                    ","+NVL(dataReader["totalfees"].ToString()) + ","+NVL(dataReader["feespaid"].ToString()) + ","+ NVL(Convert.ToDouble(dataReader["balancedue"]).ToString().Replace(",",".")) + ", "+NVL(dataReader["paychoice"].ToString()) +", "+ CheckIfUnix(dataReader["regdate"])+ ")")
                ;
            }
            pPostgres.Message = "Tbl_event_registrations - extraction - FINISH";

            MySqlDataReader dataReader2 = pMysql.Select("select id, neweventregid from registrations where neweventregid is not null and neweventregid!=0");
            pMysql.Message = "Tbl_registrations.event_registration_id - extraction - START ";

            while (dataReader2.Read())
            {
                pPostgres.Update("update tbl_registrations set event_registration_id=" + dataReader2["neweventregid"].ToString() + " where id=" + dataReader2["id"].ToString());
            }

            pPostgres.Message = "Tbl_registrations.event_registration_id - extraction - FINISH";

        }

        public string CheckIfUnix(object data)
        {
            if (data.GetType().Name == "DBNull" || data == null || data.ToString() == "")
            {
                return "null";
            }
            string stringData = data.ToString();
            if (stringData.IndexOf('/') != -1)
            {
                return "'"+data.ToString()+"'";
            } else {
                return "'" + FromUnixTime(Convert.ToInt64(data)).ToString().Replace(". ", ".") + "'";
            }
        }

        public string GetStudioId(string pStudioId, string pStudio_name, PostgreSQL_DB pPostgres)
        {
            if (!String.IsNullOrEmpty(pStudioId))
            {
                return pStudioId;
            }
            if (String.IsNullOrEmpty(pStudio_name))
            {
                return "null";
            }
            else
            {
                string studioId =
                    GetId("select id from tbl_studios where name like '" + pStudio_name.Replace("'", "''") + "'",
                        pPostgres);
                if (studioId != "dummy")
                {
                    return studioId;
                }
                else
                {
                    return AddNewStudio(pStudio_name, "null", pPostgres);
                }
            }
        }

        private string GetAddressId(string pAddress, string pCity, string pState, string pZip, string pCountryId,
            PostgreSQL_DB pPostgres)
        {
            NpgsqlDataReader query;
            query = pPostgres.Select("select distinct id " +
                                     "from tbl_addresses where address like '" + pAddress.Replace("'", "''") +
                                     "' and city = '" + pCity.Replace("'", "''") + "' and zip like '" + pZip + "' and country_id=" +NVL(pCountryId) + ";");
            string pom;
            while (query.Read())
            {
                pom = query[0].ToString();
                query.Dispose();
                return "'" + pom + "'";
            }
            query.Dispose();
            if (pAddress != "")
            {
                string pomStateId = GetId("select id from tbl_states where name like '" + pState + "'", pPostgres);
                pPostgres.Insert("insert into tbl_addresses(state_id, address, city, zip) values(" + pomStateId +
                                 ",'" + pAddress.Replace("'", "''") + "','" + pCity.Replace("'", "''") + "','" + pZip + "');");
                string p_address_id = GetId("select max(id) from tbl_addresses", pPostgres);
                return p_address_id;
            }
            else
            {
                return "null";
            }
        }

        private string GetPersonId(string pAddress, string pCity, string pState, string pZip, string pCountryId,
            string pFname, string pLname, string pEmail,
            string pContactType, string pPhone, string pPhone2, PostgreSQL_DB pPostgres)
        {
            string AddressId = GetAddressId(pAddress, pCity, pState, pZip, pCountryId, pPostgres);
            string GenderId = "null";
            string PersonType = GetId("select id from tbl_person_types where name like '" + pContactType + "' limit 1;", pPostgres);
            int Max_person_id = Convert.ToInt32(GetId("select max(id) from tbl_persons", pPostgres));

            // into person
            string birthdate = "null";
            pPostgres.Insert(
                "insert into tbl_persons(id, address_id, gender_id, fname, lname, birthdate, person_type_id) " +
                "values("+ ++Max_person_id +","  + AddressId + ", " + GenderId + ",'" + pFname.Replace("'", "''") + "'," +
                "'" + pLname.Replace("'", "''") + "'," + birthdate + "," + PersonType + ");");

            string phone_1 = pPhone;
            string phone_2 = pPhone2;
            var c = new[] {'(', ')', '-', ' '};
            // insert into studio_has_contact_type
            if (phone_1 != "")
            {
                pPostgres.Insert("insert into person_contact_info(person_id, contact_type_id, value)" +
                                 "values(" + Max_person_id + ",2,'" + Remove(phone_1, c) + "')");
            }
            if (phone_2 != "")
            {
                pPostgres.Insert("insert into person_contact_info(person_id, contact_type_id, value)" +
                                 "values(" + Max_person_id + ",2,'" + Remove(phone_2, c) + "')");
            }
            if (pEmail != "")
            {
                pPostgres.Insert("insert into person_contact_info(person_id, contact_type_id, value)" +
                                 "values(" + Max_person_id + ",1,'" + pEmail.ToLower().Replace("'", "''") + "')");
            }
            return Max_person_id.ToString();
        }
        private string Get_json_date(string confirm, string reg)
        {
            dynamic dates = new JObject();
            dates.confirm = confirm;
            dates.reg = reg;
            return dates.ToString();
        }
        public string NVL2(string pParam)
        {
            if (pParam == "" || pParam == "0" || pParam == "null" || pParam == " ")
            {
                return "null";
            }
            return "'" + pParam + "'";
        }
    }
}