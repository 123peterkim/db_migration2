﻿using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_playlist_workshop_levels: BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select name from tbl_playlist_workshop_levels " +
                                                       "union select name from tbl_age_divisions " +
                                                       "union select name from tbl_workshop_levels_14 " +
                                                       "union select name from tbl_workshop_levels_17 " +
                                                       "union select name from tbl_workshop_levels_20 " +
                                                       "union select name from tbl_workshop_levels_22 ;");
            pMysql.Message = "tbl_playlist_workshop_levels - extraction - START";
            int counter = 0;
            while (dataReader.Read())
            {
                var eventId = "null";
                if (dataReader[0].ToString() == "JUMPstart" || dataReader[0].ToString() == "JUMPstarts")
                {
                    eventId = "7";
                }
                if (dataReader[0].ToString() == "Nubies" || dataReader[0].ToString() == "Nubie")
                {
                    eventId = "8";
                }
                if (dataReader[0].ToString() == "PeeWee")
                {
                    eventId = "14";
                }
                if (dataReader[0].ToString() == "Sidekicks" || dataReader[0].ToString() == "Sidekick")
                {
                    eventId = "18";
                }
                if (dataReader[0].ToString() == "Rookies" || dataReader[0].ToString() == "Rookie")
                {
                    eventId = "28";
                }
                pPostgres.Insert("insert into tbl_levels(id, name, event_id) values(" + ++counter + ",'" + dataReader[0] + "', "+eventId+" )");
            }
            pPostgres.Insert("insert into tbl_levels(id, name) values(0,'DUMMY')");
            pPostgres.Message = "tbl_playlist_workshop_levels - extraction - FINISH";
        }
    }
}