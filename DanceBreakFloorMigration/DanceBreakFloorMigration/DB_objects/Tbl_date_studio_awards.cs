﻿using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Date_studio_awards : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from tbl_date_studio_awards");

            pMysql.Message = "date_studio_awards - extraction - START";
            while (dataReader.Read())
            {
                string date_studio_id = GetId("select id from date_studios where studio_id=" + dataReader["studioid"]+ " and tour_date_id=" + dataReader["tourdateid"], pPostgres);

                pPostgres.Insert("insert into date_studio_awards(date_studio_id, studio_award_id, winner) " +
                                 "values(" + date_studio_id + ",'" + dataReader["awardtypeid"] + "','"+CheckBool(dataReader["winner"].ToString()) + "')");
            }
            pPostgres.Message = "date_studio_awards - extraction - FINISH";
        }
    }
}