﻿using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Store_products_sizes: BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from store_product_sizes;");
            pMysql.Message = "store_products_sizes - extraction - START";
            while (dataReader.Read())
            {
                pPostgres.Insert("insert into store_products_sizes(store_product_id,store_size_id) values('" + dataReader["productid"] +"','"+dataReader["sizeid"] +"')");
            }
            pPostgres.Message = "store_products_sizes - extraction - FINISH";
        }
    }
}