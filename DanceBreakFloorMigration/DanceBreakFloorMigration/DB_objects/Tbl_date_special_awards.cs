﻿using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Date_special_awards : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from tbl_date_special_awards");

            pMysql.Message = "date_special_awards - extraction - START";
            int pcounter = 0;
            while (dataReader.Read())
            {
                pPostgres.Insert("insert into date_special_awards(id, tour_date_id, date_routine_id, special_award_id) " +
                                 "values('" + dataReader["id"] + "','" + dataReader["tourdateid"] + "','" + dataReader["dateroutineid"] + "','" + dataReader["awardid"] + "')");
            }
            pPostgres.Message = "date_special_awards - extraction - FINISH";
        }
    }
}