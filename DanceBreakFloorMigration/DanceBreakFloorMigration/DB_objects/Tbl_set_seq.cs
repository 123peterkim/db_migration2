using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_set_seq : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            pPostgres.Message = "Tbl_set_seq - extraction - START";
            pPostgres.Insert("SELECT setval('date_dancers_id_seq', (SELECT MAX(id) FROM date_dancers))");
            pPostgres.Insert("SELECT setval('date_mybtf_exceptions_id_seq', (SELECT MAX(id) FROM date_mybtf_exceptions))");
            pPostgres.Insert("SELECT setval('date_playlists_id_seq', (SELECT MAX(id) FROM date_playlists))");
            pPostgres.Insert("SELECT setval('date_routines_id_seq', (SELECT MAX(id) FROM date_routines))");
            pPostgres.Insert("SELECT setval('date_schedule_competitions_id_seq', (SELECT MAX(id) FROM date_schedule_competitions))");
            pPostgres.Insert("SELECT setval('date_schedule_workshops_id_seq', (SELECT MAX(id) FROM date_schedule_workshops))");
            pPostgres.Insert("SELECT setval('date_scholarships_id_seq', (SELECT MAX(id) FROM date_scholarships))");
            pPostgres.Insert("SELECT setval('date_special_awards_id_seq', (SELECT MAX(id) FROM date_special_awards))");
            pPostgres.Insert("SELECT setval('date_studios_id_seq', (SELECT MAX(id) FROM date_studios))");
            pPostgres.Insert("SELECT setval('hotel_contact_info_id_seq', (SELECT MAX(id) FROM hotel_contact_info))");
            pPostgres.Insert("SELECT setval('person_contact_info_id_seq', (SELECT MAX(id) FROM person_contact_info))");
            pPostgres.Insert("SELECT setval('registrations_routines_dancers_id_seq', (SELECT MAX(id) FROM registrations_routines_dancers))");
            pPostgres.Insert("SELECT setval('studio_contact_info_id_seq', (SELECT MAX(id) FROM studio_contact_info))");
            pPostgres.Insert("SELECT setval('tbl_addresses_id_seq', (SELECT MAX(id) FROM tbl_addresses))");
            pPostgres.Insert("SELECT setval('tbl_admins_id_seq', (SELECT MAX(id) FROM tbl_admins))");
            pPostgres.Insert("SELECT setval('tbl_age_divisions_id_seq', (SELECT MAX(id) FROM tbl_age_divisions))");
            pPostgres.Insert("SELECT setval('tbl_award_types_id_seq', (SELECT MAX(id) FROM tbl_award_types))");
            pPostgres.Insert("SELECT setval('tbl_categories_id_seq', (SELECT MAX(id) FROM tbl_categories))");
            pPostgres.Insert("SELECT setval('tbl_vip_types_id_seq', (SELECT MAX(id) FROM tbl_vip_types))");
            pPostgres.Insert("SELECT setval('tbl_competition_awards_id_seq', (SELECT MAX(id) FROM tbl_competition_awards))");
            pPostgres.Insert("SELECT setval('tbl_competition_cash_awards_id_seq', (SELECT MAX(id) FROM tbl_competition_cash_awards))");
            pPostgres.Insert("SELECT setval('tbl_contact_types_id_seq', (SELECT MAX(id) FROM tbl_contact_types))");
            pPostgres.Insert("SELECT setval('tbl_countries_id_seq', (SELECT MAX(id) FROM tbl_countries))");
            pPostgres.Insert("SELECT setval('tbl_dancers_id_seq', (SELECT MAX(id) FROM tbl_dancers))");
            pPostgres.Insert("SELECT setval('tbl_dts_attendees_id_seq', (SELECT MAX(id) FROM tbl_dts_attendees))");
            pPostgres.Insert("SELECT setval('tbl_dts_fees_id_seq', (SELECT MAX(id) FROM tbl_dts_fees))");
            pPostgres.Insert("SELECT setval('tbl_dts_reg_types_id_seq', (SELECT MAX(id) FROM tbl_dts_reg_types))");
            pPostgres.Insert("SELECT setval('tbl_dts_registrations_id_seq', (SELECT MAX(id) FROM tbl_dts_registrations))");
            pPostgres.Insert("SELECT setval('tbl_event_attendees_id_seq', (SELECT MAX(id) FROM tbl_event_attendees))");
            pPostgres.Insert("SELECT setval('tbl_event_cities_id_seq', (SELECT MAX(id) FROM tbl_event_cities))");
            pPostgres.Insert("SELECT setval('tbl_event_reg_type_names_id_seq', (SELECT MAX(id) FROM tbl_event_reg_type_names))");
            pPostgres.Insert("SELECT setval('tbl_event_reg_types_id_seq', (SELECT MAX(id) FROM tbl_event_reg_types))");
            pPostgres.Insert("SELECT setval('tbl_event_registrations_id_seq', (SELECT MAX(id) FROM tbl_event_registrations))");
            pPostgres.Insert("SELECT setval('tbl_event_types_id_seq', (SELECT MAX(id) FROM tbl_event_types))");
            pPostgres.Insert("SELECT setval('tbl_events_id_seq', (SELECT MAX(id) FROM tbl_events))");
            pPostgres.Insert("SELECT setval('tbl_faculty_id_seq', (SELECT MAX(id) FROM tbl_faculty))");
            pPostgres.Insert("SELECT setval('tbl_gender_id_seq', (SELECT MAX(id) FROM tbl_gender))");
            pPostgres.Insert("SELECT setval('tbl_hotels_id_seq', (SELECT MAX(id) FROM tbl_hotels))");
            pPostgres.Insert("SELECT setval('tbl_jobs_id_seq', (SELECT MAX(id) FROM tbl_jobs))");
            pPostgres.Insert("SELECT setval('tbl_mybtf_user_hearts_id_seq', (SELECT MAX(id) FROM tbl_mybtf_user_hearts))");
            pPostgres.Insert("SELECT setval('tbl_mybtf_user_stats_id_seq', (SELECT MAX(id) FROM tbl_mybtf_user_stats))");
            pPostgres.Insert("SELECT setval('tbl_mybtf_users_id_seq', (SELECT MAX(id) FROM tbl_mybtf_users))");
            pPostgres.Insert("SELECT setval('tbl_online_critique_access_codes_id_seq', (SELECT MAX(id) FROM tbl_online_critique_access_codes))");
            pPostgres.Insert("SELECT setval('tbl_online_critiques_id_seq', (SELECT MAX(id) FROM tbl_online_critiques))");
            pPostgres.Insert("SELECT setval('tbl_online_scoring_id_seq', (SELECT MAX(id) FROM tbl_online_scoring))");
            pPostgres.Insert("SELECT setval('tbl_online_scoring_data_id_seq', (SELECT MAX(id) FROM tbl_online_scoring_data))");
            pPostgres.Insert("SELECT setval('tbl_payment_methods_id_seq', (SELECT MAX(id) FROM tbl_payment_methods))");
            pPostgres.Insert("SELECT setval('tbl_perf_div_types_id_seq', (SELECT MAX(id) FROM tbl_perf_div_types))");
            pPostgres.Insert("SELECT setval('tbl_performance_divisions_id_seq', (SELECT MAX(id) FROM tbl_performance_divisions))");
            pPostgres.Insert("SELECT setval('tbl_person_types_id_seq', (SELECT MAX(id) FROM tbl_person_types))");
            pPostgres.Insert("SELECT setval('tbl_persons_id_seq', (SELECT MAX(id) FROM tbl_persons))");
            pPostgres.Insert("SELECT setval('tbl_levels_id_seq', (SELECT MAX(id) FROM tbl_levels))");
            pPostgres.Insert("SELECT setval('tbl_promo_codes_id_seq', (SELECT MAX(id) FROM tbl_promo_codes))");
            pPostgres.Insert("SELECT setval('tbl_promo_codes_types_id_seq', (SELECT MAX(id) FROM tbl_promo_codes_types))");
            pPostgres.Insert("SELECT setval('tbl_registrations_id_seq', (SELECT MAX(id) FROM tbl_registrations))");
            pPostgres.Insert("SELECT setval('tbl_registrations_attendees_dts_id_seq', (SELECT MAX(id) FROM tbl_registrations_attendees_dts))");
            pPostgres.Insert("SELECT setval('tbl_registrations_best_dancers_id_seq', (SELECT MAX(id) FROM tbl_registrations_best_dancers))");
            pPostgres.Insert("SELECT setval('tbl_registrations_dancers_id_seq', (SELECT MAX(id) FROM tbl_registrations_dancers))");
            pPostgres.Insert("SELECT setval('tbl_registrations_routines_id_seq', (SELECT MAX(id) FROM tbl_registrations_routines))");
            pPostgres.Insert("SELECT setval('tbl_registrations_soty_id_seq', (SELECT MAX(id) FROM tbl_registrations_soty))");
            pPostgres.Insert("SELECT setval('tbl_registrations_specialty_id_seq', (SELECT MAX(id) FROM tbl_registrations_specialty))");
            pPostgres.Insert("SELECT setval('tbl_routine_categories_id_seq', (SELECT MAX(id) FROM tbl_routine_categories))");
            pPostgres.Insert("SELECT setval('tbl_routine_types_id_seq', (SELECT MAX(id) FROM tbl_routine_types))");
            pPostgres.Insert("SELECT setval('tbl_routines_id_seq', (SELECT MAX(id) FROM tbl_routines))");
            pPostgres.Insert("SELECT setval('tbl_scholarships_id_seq', (SELECT MAX(id) FROM tbl_scholarships))");
            pPostgres.Insert("SELECT setval('tbl_seasons_id_seq', (SELECT MAX(id) FROM tbl_seasons))");
            pPostgres.Insert("SELECT setval('tbl_songs_id_seq', (SELECT MAX(id) FROM tbl_songs))");
            pPostgres.Insert("SELECT setval('tbl_soty_types_id_seq', (SELECT MAX(id) FROM tbl_soty_types))");
            pPostgres.Insert("SELECT setval('tbl_special_awards_id_seq', (SELECT MAX(id) FROM tbl_special_awards))");
            pPostgres.Insert("SELECT setval('tbl_staff_id_seq', (SELECT MAX(id) FROM tbl_staff))");
            pPostgres.Insert("SELECT setval('tbl_staff_types_id_seq', (SELECT MAX(id) FROM tbl_staff_types))");
            pPostgres.Insert("SELECT setval('tbl_states_id_seq', (SELECT MAX(id) FROM tbl_states))");
            pPostgres.Insert("SELECT setval('tbl_store_colors_id_seq', (SELECT MAX(id) FROM tbl_store_colors))");
            pPostgres.Insert("SELECT setval('tbl_store_giftcards_id_seq', (SELECT MAX(id) FROM tbl_store_giftcards))");
            pPostgres.Insert("SELECT setval('tbl_store_hearts_id_seq', (SELECT MAX(id) FROM tbl_store_hearts))");
            pPostgres.Insert("SELECT setval('tbl_store_orders_id_seq', (SELECT MAX(id) FROM tbl_store_orders))");
            pPostgres.Insert("SELECT setval('tbl_store_product_subtypes_id_seq', (SELECT MAX(id) FROM tbl_store_product_subtypes))");
            pPostgres.Insert("SELECT setval('tbl_store_product_types_id_seq', (SELECT MAX(id) FROM tbl_store_product_types))");
            pPostgres.Insert("SELECT setval('tbl_store_products_id_seq', (SELECT MAX(id) FROM tbl_store_products))");
            pPostgres.Insert("SELECT setval('tbl_store_products_inventory_id_seq', (SELECT MAX(id) FROM tbl_store_products_inventory))");
            pPostgres.Insert("SELECT setval('tbl_store_promo_codes_id_seq', (SELECT MAX(id) FROM tbl_store_promo_codes))");
            pPostgres.Insert("SELECT setval('tbl_store_sizes_id_seq', (SELECT MAX(id) FROM tbl_store_sizes))");
            pPostgres.Insert("SELECT setval('tbl_studio_awards_id_seq', (SELECT MAX(id) FROM tbl_studio_awards))");
            pPostgres.Insert("SELECT setval('tbl_studios_id_seq', (SELECT MAX(id) FROM tbl_studios))");
            pPostgres.Insert("SELECT setval('tbl_tda_award_nominations_id_seq', (SELECT MAX(id) FROM tbl_tda_award_nominations))");
            pPostgres.Insert("SELECT setval('tbl_tda_award_types_id_seq', (SELECT MAX(id) FROM tbl_tda_award_types))");
            pPostgres.Insert("SELECT setval('tbl_tda_best_dancer_data_id_seq', (SELECT MAX(id) FROM tbl_tda_best_dancer_data))");
            pPostgres.Insert("SELECT setval('tbl_tda_peoples_choice_votes_id_seq', (SELECT MAX(id) FROM tbl_tda_peoples_choice_votes))");
            pPostgres.Insert("SELECT setval('tbl_tour_dates_id_seq', (SELECT MAX(id) FROM tbl_tour_dates))");
            pPostgres.Insert("SELECT setval('tbl_venues_id_seq', (SELECT MAX(id) FROM tbl_venues))");
            pPostgres.Insert("SELECT setval('tbl_waivers_id_seq', (SELECT MAX(id) FROM tbl_waivers))");
            pPostgres.Insert("SELECT setval('tbl_workshop_levels_id_seq', (SELECT MAX(id) FROM tbl_workshop_levels))");
            pPostgres.Insert("SELECT setval('tbl_workshop_rooms_id_seq', (SELECT MAX(id) FROM tbl_workshop_rooms))");
            pPostgres.Insert("SELECT setval('venue_contact_info_id_seq', (SELECT MAX(id) FROM venue_contact_info))");
            pPostgres.Insert("SELECT setval('tbl_competition_groups_id_seq', (SELECT MAX(id) FROM tbl_competition_groups))");
            pPostgres.Insert("SELECT setval('tbl_online_scoring_attributes_id_seq', (SELECT MAX(id) FROM tbl_online_scoring_attributes))");
            pPostgres.Insert("SELECT setval('tbl_teachers_id_seq', (SELECT MAX(id) FROM tbl_teachers))");
            pPostgres.Message = "Tbl_set_seq - extraction - FINISH";
        }
    }
}

