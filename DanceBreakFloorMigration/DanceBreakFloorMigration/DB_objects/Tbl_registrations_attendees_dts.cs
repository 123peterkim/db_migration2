﻿using System;
using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_registrations_attendees_dts : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from tbl_user_registrations_attendees_dts;");
            pMysql.Message = "tbl_registrations_attendees_dts - extraction - START ";
            while (dataReader.Read())
            {
                int maxid = Convert.ToInt32(dataReader["regid"].ToString())+500000;

                string RegId = GetId("select id from tbl_registrations where id='" + maxid + "' limit 1;", pPostgres);

                pPostgres.Insert("insert into tbl_registrations_attendees_dts(id, registration_id, mybtf_user_id, dts_reg_type_id, promo_code_id, fee) " +
                                 "values("+dataReader["id"]+", "+RegId+"," +
                                 ""+ UserIdManage(dataReader["fname"].ToString().Replace("'","''"), dataReader["lname"].ToString().Replace("'", "''"), dataReader["email"].ToString(), dataReader["affiliation"].ToString(), pPostgres) + "," +
                                 ""+dataReader["regtypeid"] +", "+ GetPromoCodeId(NVLJSON(dataReader["promocode"].ToString()), pPostgres) +","+dataReader["fee"]+");");
            }

            pPostgres.Message = "tbl_registrations_attendees_dts - extraction - FINISH";
        }

        private string GetPromoCodeId(string promoCode, PostgreSQL_DB pPostgres)
        {
            string PromoCodeId = GetId("select id from tbl_promo_codes where name like '"+ promoCode.ToUpper() +"' limit 1;", pPostgres);
            if (PromoCodeId != "null") {
                return "'"+PromoCodeId+"'";
            }
            return "null";
        }

        private string UserIdManage(string pname, string lname, string pemail, string pAffiliation, PostgreSQL_DB pPostgres)
        {
            string UserId = GetId("select id from tbl_mybtf_users where email like '" + pemail + "' limit 1;", pPostgres);
            string personType = GetId("select id from tbl_person_types where name like '" + pAffiliation + "' limit 1;", pPostgres);
            if (UserId == "null")
            {
                int Max_person_id = Convert.ToInt32(GetId("select max(id) from tbl_persons", pPostgres));
                pPostgres.Insert("insert into tbl_persons(id, address_id, gender_id, fname, lname, birthdate, person_type_id) " +
                                 "values('"+ ++Max_person_id + "',null,null,'" + pname + "','" + lname + "',null, " + personType + ") ");

                int Max_mybtf_user_id = Convert.ToInt32(GetId("select max(id) from tbl_mybtf_users", pPostgres));
                pPostgres.Insert("insert into tbl_mybtf_users(id, email, password, active, person_id, unregistered) " +
                                 "values('" + ++Max_mybtf_user_id + "','" + pemail + "',null,null,'" + Max_person_id + "','1')");

                UserId = Max_mybtf_user_id.ToString();
            }
            return UserId;
        }
    }
}