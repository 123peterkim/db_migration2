﻿using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_studio_awards : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from tbl_studio_awards");

            pMysql.Message = "Tbl_studio_awards - extraction - START";
            int pcounter = 0;
            while (dataReader.Read())
            {
                string p_award_name = GetId("select id from tbl_award_types where name like '" + dataReader["awardname"].ToString().Replace("'","''")  + "' limit 1", pPostgres);

                pPostgres.Insert("insert into Tbl_studio_awards(id, event_id, award_type_id) " +
                                 "values('"+dataReader["id"]+ "','" + dataReader["eventid"] + "','"+p_award_name+"')");
            }
            pPostgres.Message = "Tbl_studio_awards - extraction - FINISH";
        }
    }
}