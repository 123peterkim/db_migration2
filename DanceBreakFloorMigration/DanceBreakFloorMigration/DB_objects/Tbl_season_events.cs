﻿using System;
using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Seasons_events: BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select id, seasonid, eventid from tbl_season_events;");
            pMysql.Message = "seasons_events - extraction - START";
            while (dataReader.Read())
            {
                    pPostgres.Insert("insert into seasons_events(season_id, event_id) " +
                                 "values('" + dataReader[1] + "','" + dataReader[2] + "')");
            }
            pPostgres.Message = "seasons_events - extraction - FINISH";
        }
    }
}