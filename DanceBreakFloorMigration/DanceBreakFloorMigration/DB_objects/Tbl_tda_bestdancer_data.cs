﻿using System;
using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_tda_best_dancer_data : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from tbl_tda_bestdancer_data;");
            pMysql.Message = "tbl_tda_best_dancer_data - extraction - START";
            while (dataReader.Read())
            {
                // -------------------
                if (!String.IsNullOrEmpty(dataReader["routineid"].ToString()))
                {
                    CreateDummyRoutines(NVL(dataReader["routineid"].ToString()), pPostgres);
                }

                string pChoreographer = "0";
                if (!String.IsNullOrEmpty(dataReader["choreographer"].ToString()))
                {
                    pChoreographer = AddNewPerson(dataReader["choreographer"].ToString(), pPostgres);
                }
                pPostgres.Insert("insert into tbl_tda_best_dancer_data(id, tour_date_id, dancer_id, routine_id, choreographer_id, studio_id, is_competing, " +
                                 "jacket, has_photo, ballet, danceoff, group_id, perc, place, jazz) " +
                                 "values("+dataReader["id"]+ "," + NVL(dataReader["tourdateid"].ToString()) + "," + NVL(dataReader["profileid"].ToString()) + "," +
                                 "" + NVL(dataReader["routineid"].ToString()) + "," + pChoreographer + "," + NVL(dataReader["studioid"].ToString()) + "," +
                                 "" + dataReader["iscompeting"] + ",'" + Get_json_jacket(dataReader["jacketname"].ToString().Replace("'","''"), dataReader["jacketsize"].ToString().Replace("'", "''")) + "'," +
                                 "" + CheckBool(dataReader["hasphoto"].ToString()) + "," + NVL(dataReader["ballet"].ToString()) + "," +
                                 "'"+Get_json_danceoff(dataReader["danceoff"].ToString(), dataReader["danceoff_max"].ToString()) +"'," +
                                 "" + NVL(dataReader["groupid"].ToString()) + "," +
                                 "'"+Get_json_perc(dataReader["perc_solo"].ToString(), dataReader["perc_ballet"].ToString(), dataReader["perc_danceoff"].ToString(), dataReader["perc_round2"].ToString(), dataReader["perc_total"].ToString()) +"'," +
                                 "'"+Get_json_place(dataReader["round1_place"].ToString(), dataReader["round2_place"].ToString(), dataReader["round3_place"].ToString()) +"'," + NVL(dataReader["jazz"].ToString()) + ");");
            }
            pPostgres.Message = "tbl_tda_best_dancer_data - extraction - FINISH";
        }
        public string AddNewPerson(string pName, PostgreSQL_DB pPostgres)
        {
            string personName = pName.ToString();
            int idx = personName.LastIndexOf(' ');
            string PersonType = GetId("select id from tbl_person_types where name like 'Teacher' limit 1;", pPostgres);
            int Max_person_id = Convert.ToInt32(GetId("select max(id) from tbl_persons", pPostgres));

            if (idx != -1)
            {
                pPostgres.Insert("insert into tbl_persons(id, fname, lname, person_type_id) " +
                                 "values("+ ++Max_person_id +",'" + personName.Substring(0, idx).Replace("'","''") + "','" + personName.Substring(idx + 1).Replace("'","''") + "'," + PersonType + ");");
                return Max_person_id.ToString();
            } else
            {
                pPostgres.Insert("insert into tbl_persons(id, fname, lname, person_type_id) " +
                                 "values("+ ++Max_person_id +",'" + personName.Replace("'","''") + "','" + "null" + "'," + PersonType + ");");
                return Max_person_id.ToString();
            }

        }
        private string Get_json_danceoff(string pdanceoff, string pdanceoff_max)
        {
            dynamic danceoff = new JObject();
            danceoff.score = Convert.ToInt32(NVLINT(pdanceoff));
            danceoff.max = Convert.ToInt32(NVLINT(pdanceoff_max));
            return danceoff.ToString();
        }

        private string Get_json_perc(string perc_solo, string perc_ballet, string perc_danceoff, string perc_round2, string perc_total)
        {
            dynamic perc = new JObject();
            perc.solo = Math.Round(Convert.ToDecimal(NVLINT(perc_solo)), 2);
            perc.ballet = Math.Round(Convert.ToDecimal(NVLINT(perc_ballet)), 2);
            perc.danceoff = Math.Round(Convert.ToDecimal(NVLINT(perc_danceoff)), 2);
            perc.round2 = Math.Round(Convert.ToDecimal(NVLINT(perc_round2)), 2);
            perc.total = Math.Round(Convert.ToDecimal(NVLINT(perc_total)), 2);
            return perc.ToString();
        }

        private string Get_json_place(string round1_place, string round2_place, string round3_place)
        {
            dynamic place = new JObject();
            place.round1 = Convert.ToInt32(NVLINT(round1_place));
            place.round2 = Convert.ToInt32(NVLINT(round2_place));
            place.round3 = Convert.ToInt32(NVLINT(round3_place));
            return place.ToString();
        }

        private string Get_json_jacket(string name, string size)
        {
            dynamic jacket = new JObject();
            jacket.name = name;
            jacket.size = size;
            return "" + jacket.ToString() + "";
        }
    }
}