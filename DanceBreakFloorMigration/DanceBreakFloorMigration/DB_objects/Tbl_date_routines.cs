﻿using System;
using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;
using Npgsql;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Date_routines : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from tbl_date_routines;");
            pMysql.Message = "date_routines - extraction - START";
            while (dataReader.Read())
            {
                string award_typename= string.IsNullOrWhiteSpace(dataReader["award_typename"].ToString()) ? "null" : "'"+dataReader["award_typename"].ToString().Replace("'", "''") + "'";
                string fee = string.IsNullOrWhiteSpace(dataReader["fee"].ToString()) ? "null" : dataReader["fee"].ToString();
                string comp_group_id = GetCompGroupId(dataReader["tourdateid"].ToString(), pPostgres);

                
                pPostgres.Insert("insert into date_routines(id, tour_date_id, routine_id, studio_id, age_division_id, category_id, " +
                                 "performance_division_id, perf_div_type_id, routine_type_id, fees, canceled, custom_dancer_count, duration, " +
                                 "place, award_name, uploaded_duration, extra_time, uploaded, competition_groups) " +
                                 "values("+dataReader["id"] + "," + dataReader["tourdateid"] + "," + dataReader["routineid"] + "," + dataReader["studioid"] + "," + dataReader["agedivisionid"] + "," +
                                 "" + dataReader["routinecategoryid"] + ","+ dataReader["perfcategoryid"] + " , "+ CheckBool(dataReader["perfdivtype"].ToString()) + "," + dataReader["routinetypeid"] + "," + NVL(Get_json_fee(fee, CheckBool(dataReader["custom_fee"].ToString()))) + "," +CheckBool(dataReader["canceled"].ToString()) + "," +
                                 "" + dataReader["custom_dancer_count"] + "," + dataReader["duration"] + ","  + NVL(Get_json_place(dataReader["place_hsa"].ToString(), dataReader["place_hsp"].ToString())) + "," +
                                 "" + award_typename + ",'" + dataReader["uploaded_duration"] + "'," + dataReader["extra_time"] + "," +CheckBool(dataReader["uploaded"].ToString()) + "" +
                                 ",'"+Get_competition_groups(CheckBool(dataReader["prelims"].ToString()),CheckBool(dataReader["vips"].ToString()),CheckBool(dataReader["finals"].ToString()),  comp_group_id.ToString()) +"')");

                string hello = Get_json_Prelims(dataReader["id"].ToString(), "1", dataReader["prelims"].ToString(), dataReader["prelims_score1"].ToString(), dataReader["prelims_score2"].ToString(), dataReader["prelims_score3"].ToString(), dataReader["prelims_score4"].ToString(),
                                 dataReader["prelims_score5"].ToString(), dataReader["prelims_score6"].ToString(), dataReader["prelims_awardid"].ToString(), dataReader["prelims_total_score"].ToString(), dataReader["prelims_dropped_score"].ToString(),
                                 CheckIfExists(dataReader["prelims_time"]), dataReader["number_prelims"].ToString(), dataReader["prelims_has_a"].ToString(), dataReader["prelims_dropped_score2"].ToString(),
                                 dataReader["room_prelims"].ToString(), pPostgres);
                string hello2 = Get_json_VIP(dataReader["id"].ToString(), comp_group_id.ToString(), dataReader["vips"].ToString(), dataReader["vips_score1"].ToString(), dataReader["vips_score2"].ToString(), dataReader["vips_score3"].ToString(),
                                  dataReader["vips_score4"].ToString(), dataReader["vips_score5"].ToString(), dataReader["vips_score6"].ToString(), dataReader["vips_awardid"].ToString(),
                                  dataReader["vips_dropped_score"].ToString(), dataReader["vips_total_score"].ToString(), CheckIfExists(dataReader["vips_time"]), dataReader["vips_dropped_score2"].ToString(),
                                  dataReader["number_vips"].ToString(), dataReader["vips_has_a"].ToString(), dataReader["room_vips"].ToString(), pPostgres);
                string hello3 = Get_json_final(dataReader["id"].ToString(), "2", dataReader["finals"].ToString(), dataReader["finals_score1"].ToString(), dataReader["finals_score2"].ToString(), dataReader["finals_score3"].ToString(),
                                 dataReader["finals_score4"].ToString(), dataReader["finals_score5"].ToString(), dataReader["finals_score6"].ToString(), dataReader["finals_awardid"].ToString(),
                                 dataReader["finals_total_score"].ToString(), dataReader["finals_dropped_score"].ToString(), CheckIfExists(dataReader["finals_time"]), dataReader["number_finals"].ToString(),
                                 dataReader["finals_dropped_score2"].ToString(), dataReader["finals_has_a"].ToString(), dataReader["room_finals"].ToString(), pPostgres);

            }
            pPostgres.Message = "date_routines - extraction - FINISH";
        }

        private string GetCompGroupId(string tour_date_id, PostgreSQL_DB pPostgres)
                {
                    string event_id = GetId("select event_id from tbl_tour_dates where id="+tour_date_id+";", pPostgres);
                    if (event_id == "14")
                    {
                        return "3";
                    } else
                    {
                        return "4";
                    }
                }

        private string Get_competition_groups(int prelims, int vip, int finals, string group_id)
        {
            dynamic groups_data = new JObject();
            groups_data.finals = finals;
            groups_data.prelims = prelims;
            if (group_id == "3")
            {
                groups_data.vip = vip;
            } else
            {
                groups_data.vip = vip;
            }
            return groups_data.ToString();
        }

        private string CheckIfExists(object data)
        {
            if (data.GetType().Name == "DBNull" || data == null)
            {
                return "null";
            } else {
                return FromUnixTime(Convert.ToInt64(data)).ToString().Replace(". ", ".");
            }
        }

        private string Get_json_final(string date_routine_id, string comp_group_id, string pfinals, string pfinals_score1, string pfinals_score2, string pfinals_score3, string pfinals_score4, string pfinals_score5, string pfinals_score6, string pfinals_awardid, string pfinals_total_score, string pfinals_dropped_score, string pfinals_time, string pnumber_finals, string pfinals_dropped_score2, string pfinals_has_a, string proom_finals, PostgreSQL_DB pPostgres)
        {
            if (pfinals=="True")
            {
                dynamic finals_data = new JObject();
                //finals.finals = pfinals;
                finals_data.score=new JArray();
                finals_data.dropped_score = new JArray();
                JArray x = new JArray();
                JArray y = new JArray();

                x.Add(Convert.ToInt32(NVLINT(pfinals_score1)));
                x.Add(Convert.ToInt32(NVLINT(pfinals_score2)));
                x.Add(Convert.ToInt32(NVLINT(pfinals_score3)));
                x.Add(Convert.ToInt32(NVLINT(pfinals_score4)));
                x.Add(Convert.ToInt32(NVLINT(pfinals_score5)));
                x.Add(Convert.ToInt32(NVLINT(pfinals_score6)));

                y.Add(Convert.ToInt32(NVLINT(pfinals_dropped_score)));
                y.Add(Convert.ToInt32(NVLINT(pfinals_dropped_score2)));
                finals_data.score = x;
                finals_data.dropped_score = y;
                finals_data.award_id = Convert.ToInt32(NVLJSON2(pfinals_awardid));
                finals_data.total_score = Convert.ToInt32(NVLINT(pfinals_total_score));
                if (pfinals_time == "null")
                {
                    finals_data.time = "null";
                } else
                {
                    finals_data.time = "'"+pfinals_time+"'";
                }
                finals_data.number = Convert.ToInt32(NVLINT(pnumber_finals));
                finals_data.has_a = Convert.ToInt32(TFV(pfinals_has_a));
                finals_data.room = Convert.ToInt32(NVLINT(proom_finals));

                if (pfinals_awardid == "null")
                {
                    finals_data.award_id = "null";
                } else
                {
                    finals_data.award_id = "'"+finals_data.award_id+"'";
                }
                if (proom_finals == "null")
                {
                    finals_data.room = "null";
                } else
                {
                    finals_data.room = "'"+finals_data.room+"'";
                }
                if (pfinals_has_a == "null")
                {
                    finals_data.has_a = "null";
                } else
                {
                    finals_data.has_a = "'"+finals_data.has_a+"'";
                }
                if (pnumber_finals == "null")
                {
                    finals_data.number = "null";
                } else
                {
                    finals_data.number = "'"+finals_data.number+"'";
                }
                if (pfinals_total_score == "null")
                {
                    finals_data.total_score = "null";
                }
                else
                {
                    finals_data.total_score = "'" + finals_data.total_score + "'";
                }

                pPostgres.Insert("insert into date_routines_scoring(date_routine_id, competition_group_id, competition_award_id, room, time, has_a, score, number, total_score, dropped_score) values("+date_routine_id+",2,"+finals_data.award_id+","+finals_data.room+","+finals_data.time+","+finals_data.has_a+",'"+finals_data.score.ToString().Replace("[", "{").Replace("]", "}") + "',"+finals_data.number+","+finals_data.total_score+",'"+finals_data.dropped_score.ToString().Replace("[", "{").Replace("]", "}") + "');");
                return "yes";
            } else
            {
                return "null";
            }
        }

        private string Get_json_VIP(string date_routine_id, string comp_group_id, string pvips, string pvips_score1, string pvips_score2, string pvips_score3, string pvips_score4, string pvips_score5, string pvips_score6, string pvips_awardid, string pvips_dropped_score, string pvips_total_score, string pvips_time, string pvips_dropped_score2, string pnumber_vips, string pvips_has_a, string proom_vips, PostgreSQL_DB pPostgres)
        {
            if (pvips=="True")
            {
                dynamic vip_data = new JObject();
                //vip.vips = pvips;
                vip_data.score = new JArray();
                vip_data.dropped_score = new JArray();
                JArray x = new JArray();
                x.Add(Convert.ToInt32(NVLINT(pvips_score1)));
                x.Add(Convert.ToInt32(NVLINT(pvips_score2)));
                x.Add(Convert.ToInt32(NVLINT(pvips_score3)));
                x.Add(Convert.ToInt32(NVLINT(pvips_score4)));
                x.Add(Convert.ToInt32(NVLINT(pvips_score5)));
                x.Add(Convert.ToInt32(NVLINT(pvips_score6)));

                JArray y = new JArray();
                y.Add(Convert.ToInt32(NVLINT(pvips_dropped_score)));
                y.Add(Convert.ToInt32(NVLINT(pvips_dropped_score2)));
                vip_data.dropped_score = y;
                vip_data.score = x;
                vip_data.award_id = Convert.ToInt32(NVLJSON2(pvips_awardid));
                vip_data.total_score = Convert.ToInt32(NVLINT(pvips_total_score));
                if (pvips_time == "null")
                {
                    vip_data.time = "null";
                }
                else
                {
                    vip_data.time = "'"+pvips_time+"'";
                }
                vip_data.number = Convert.ToInt32(NVLINT(pnumber_vips));
                vip_data.has_a = Convert.ToInt32(TFV(pvips_has_a));
                vip_data.room = Convert.ToInt32(NVLINT(proom_vips));
                if (pvips_awardid == "null")
                {
                    vip_data.award_id = "null";
                } else
                {
                    vip_data.award_id = "'"+vip_data.award_id+"'";
                }
                if (proom_vips == "null")
                {
                    vip_data.room = "null";
                } else
                {
                    vip_data.room = "'"+vip_data.room+"'";
                }
                if (pvips_has_a == "null")
                {
                    vip_data.has_a = "null";
                } else
                {
                    vip_data.has_a = "'"+vip_data.has_a+"'";
                }
                if (pnumber_vips == "null")
                {
                    vip_data.number = "null";
                } else
                {
                    vip_data.number = "'"+vip_data.number+"'";
                }
                if (pvips_total_score == "null")
                {
                    vip_data.total_score = "null";
                }
                else
                {
                    vip_data.total_score = "'" + vip_data.total_score + "'";
                }

                pPostgres.Insert("insert into date_routines_scoring(date_routine_id, competition_group_id, competition_award_id, room, time, has_a, score, number, total_score, dropped_score) values("+date_routine_id+","+comp_group_id+","+vip_data.award_id+","+vip_data.room+","+vip_data.time+","+vip_data.has_a+",'"+vip_data.score.ToString().Replace("[", "{").Replace("]", "}") + "',"+vip_data.number+","+vip_data.total_score+",'"+vip_data.dropped_score.ToString().Replace("[", "{").Replace("]", "}") + "');");
                return "yes";
            }
            else
            {
                return "null";
            }
        }

        private string Get_json_Prelims(string date_routine_id, string comp_group_id, string pprelims, string pprelims_score1, string pprelims_score2, string pprelims_score3, string pprelims_score4, string pprelims_score5, string pprelims_score6, string pprelims_awardid, string pprelims_total_score, string pprelims_dropped_score, string pprelims_time, string pnumber_prelims, string pprelims_has_a, string pprelims_dropped_score2, string proom_prelims, PostgreSQL_DB pPostgres)
        {
            if (pprelims=="True")
            {
                dynamic prelims_data = new JObject();
                //prelims.prelims = pprelims;
                prelims_data.score = new JArray();
                prelims_data.dropped_score = new JArray();
                JArray x = new JArray();
                JArray y = new JArray();

                x.Add(Convert.ToInt32(NVLINT(pprelims_score1)));
                x.Add(Convert.ToInt32(NVLINT(pprelims_score2)));
                x.Add(Convert.ToInt32(NVLINT(pprelims_score3)));
                x.Add(Convert.ToInt32(NVLINT(pprelims_score4)));
                x.Add(Convert.ToInt32(NVLINT(pprelims_score5)));
                x.Add(Convert.ToInt32(NVLINT(pprelims_score6)));
                prelims_data.score = x;

            
                y.Add(Convert.ToInt32(NVLINT(pprelims_dropped_score)));
                y.Add(Convert.ToInt32(NVLINT(pprelims_dropped_score2)));
                prelims_data.dropped_score = y;

                prelims_data.award_id = Convert.ToInt32(NVLINT(pprelims_awardid));
                prelims_data.total_score = Convert.ToInt32(NVLINT(pprelims_total_score));
                if (pprelims_time == "null")
                {
                    prelims_data.time = "null";
                }
                else
                {
                    prelims_data.time = "'"+pprelims_time+"'";
                }
                prelims_data.number = Convert.ToInt32(NVLINT(pnumber_prelims));
                prelims_data.has_a = Convert.ToInt32(TFV(pprelims_has_a));
                prelims_data.room = Convert.ToInt32(NVLINT(proom_prelims));
                if (pprelims_awardid == "null")
                {
                    prelims_data.award_id = "null";
                } else
                {
                    prelims_data.award_id = "'"+prelims_data.award_id+"'";
                }
                if (proom_prelims == "null")
                {
                    prelims_data.room = "null";
                } else
                {
                    prelims_data.room = "'"+prelims_data.room+"'";
                }
                if (pprelims_has_a == "null")
                {
                    prelims_data.has_a = "null";
                } else
                {
                    prelims_data.has_a = "'"+prelims_data.has_a+"'";
                }
                if (pnumber_prelims == "null")
                {
                    prelims_data.number = "null";
                } else
                {
                    prelims_data.number = "'"+prelims_data.number+"'";
                }
                if (pprelims_total_score == "null")
                {
                    prelims_data.total_score = "null";
                }
                else
                {
                    prelims_data.total_score = "'" + prelims_data.total_score + "'";
                }

                pPostgres.Insert("insert into date_routines_scoring(date_routine_id, competition_group_id, competition_award_id, room, time, has_a, score, number, total_score, dropped_score) values("+date_routine_id+",1,"+prelims_data.award_id+","+prelims_data.room+","+prelims_data.time+","+prelims_data.has_a+",'"+prelims_data.score.ToString().Replace("[", "{").Replace("]", "}") + "',"+prelims_data.number+","+prelims_data.total_score+",'"+prelims_data.dropped_score.ToString().Replace("[", "{").Replace("]", "}") + "');");
                return "yes";
            }
            else
            {
                return "null";
            }
        }

        private string Get_json_place(string hsa, string hsp)
        {
            dynamic place = new JObject();
            place.hsa = Convert.ToInt32(NVLINT(hsa));
            place.hsp = Convert.ToInt32(NVLINT(hsp));
            return place.ToString();
        }

        private string Get_json_fee(string amount, int custom)
        {
            dynamic fee = new JObject();
            fee.amount = Math.Round(Convert.ToDecimal(NVLINT(amount)), 2);
            fee.custom = custom;
            return fee.ToString();
        }

    }
}