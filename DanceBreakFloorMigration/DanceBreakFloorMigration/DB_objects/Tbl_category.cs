﻿using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_categories: BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select distinct cast(name as char) from tbl_routine_categories_11 " +
                                                       "union select distinct cast(name as char) from tbl_routine_categories_14 " +
                                                       "union select distinct cast(name as char) from tbl_routine_categories_17 " +
                                                       "union select distinct cast(name as char) from tbl_routine_categories_2 " +
                                                       "union select distinct cast(name as char) from tbl_routine_categories_20 " +
                                                       "union select distinct cast(name as char) from tbl_routine_categories_22;");
            pMysql.Message = "tbl_categories - extraction - START";
            int counter = 0;
            while (dataReader.Read())
            {
                string name = dataReader[0].ToString();
                int limit = 0;
                if (name == "Solo")
                {
                    limit = 1;
                }
                if (name == "Duo/Trio")
                {
                    limit = 3;
                }
                if (name == "Group")
                {
                    limit = 9;
                }
                if (name == "Line")
                {
                    limit = 15;
                }
                if (name == "Extended Line")
                {
                    limit = 24;
                }
                if (name == "Production")
                {
                    limit = 99;
                }
                pPostgres.Insert("insert into tbl_categories(id, name, \"limit\") values(" + ++counter + ",'" + dataReader[0] + "',"+limit+")");
            }
            pPostgres.Insert("insert into tbl_categories(id, name) values(9,'Nothing');");
            pPostgres.Message = "tbl_categories - extraction - FINISH";
        }
    }
}