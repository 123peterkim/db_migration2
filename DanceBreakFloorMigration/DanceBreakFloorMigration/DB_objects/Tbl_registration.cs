﻿using System;
using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Dynamic;
using System.Web.Script.Serialization;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_registrations : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            // tbl_registrations from dancetea.registration
            // ----------------------------------------------------------
            MySqlDataReader dataReader = pMysql.Select("select * from registrations;");
            pMysql.Message = "tbl_registrations (dancetea.registration) - extraction - START ";
            while (dataReader.Read())
            {
                dynamic data;
                try {
                    data = JsonConvert.DeserializeObject<dynamic>(dataReader["data"].ToString());
                } catch (JsonReaderException e)
                {
                        string error = e.ToString();
                        char[] splitChar = { '\'' };
                        string[] errorArray = error.Split(splitChar);
                        error = errorArray[1];
                        char[] splitChar2 = { '.' };
                        string[] errorArray2 = error.Split(splitChar2);
                    string lastItem = errorArray2[errorArray2.Length - 1];
                        string dataReaderData = dataReader["data"].ToString();
                        char[] splitChar3 = { '"' };
                    string[] dataReaderDataArray = dataReaderData.Split(splitChar3);
                    int index = Array.IndexOf(dataReaderDataArray, lastItem);
                    if (index >=0)
                    {
                        string[] newArray = new string[dataReaderDataArray.Length - 2];
                        for (int i=0, j=0; i < newArray.Length; i++, j++)
                        {
                            if (i == index)
                            {
                                j = j+2;
                            }
                            newArray[i] = dataReaderDataArray[j];
                        }
                        dataReaderDataArray = newArray;
                        string newString = String.Join("\"", dataReaderDataArray);
                        data = JsonConvert.DeserializeObject<dynamic>(newString);
                    } else
                    {

                        data = "null";
                    }
                }
                string address = "";
                string city = "";
                string state = "";
                string zip = "";
                string country = "";
                try
                {
                    address = data["Contact"]["address"].ToString();
                } catch (Exception ex)
                 {
                     if (ex is RuntimeBinderException || ex is InvalidOperationException)
                     {
                    address = "null";
                    }
                }
                try
                {
                    city = data["Contact"]["city"].ToString();
                } catch (Exception ex)
             {
                 if (ex is RuntimeBinderException || ex is InvalidOperationException)
                 {
                    city= "null";
                    }
                }
                try
                {
                    state = data["Contact"]["state"].ToString();
                } catch (Exception ex)
                 {
                     if (ex is RuntimeBinderException || ex is InvalidOperationException)
                     {
                    state= "null";
                    }
                }
                try
                {
                    zip = data["Contact"]["zip"].ToString();
                } catch (Exception ex)
                 {
                     if (ex is RuntimeBinderException || ex is InvalidOperationException)
                     {
                    zip= "null";
                    }
                }
                try
                {
                    country = data["Contact"]["countryid"].ToString();
                } catch (Exception ex)
                 {
                     if (ex is RuntimeBinderException || ex is InvalidOperationException)
                     {
                    country= "null";
                    }
                }
//                string address_id = Get_address(address, city, state, zip, country,pPostgres);

                string person_type_id = "";
                try
                {
                    person_type_id = data["Contact"]["typeid"].ToString();
                    if (person_type_id == "1")
                    {
                        person_type_id = "3";
                    }
                    if (person_type_id == "2")
                    {
                        person_type_id = "5";
                    }
                    if (person_type_id == "3")
                    {
                        person_type_id = "8";
                    }
                    if (person_type_id == "4")
                    {
                        person_type_id = "9";
                    }
                    if (person_type_id == "5")
                    {
                        person_type_id = "7";
                    }
                    if (person_type_id == "6")
                    {
                        person_type_id = "2";
                    }
                } catch (Exception ex)
                 {
                     if (ex is RuntimeBinderException || ex is InvalidOperationException)
                     {
                    person_type_id= "null";
                    }
                }

                string email = "";
                try
                {
                    email = data["Contact"]["email"].ToString();
                } catch (Exception ex)
                 {
                     if (ex is RuntimeBinderException || ex is InvalidOperationException)
                     {
                    email= "null";
                    }
                }

                string phone = "";
                try
                {
                    phone = data["Contact"]["phone"].ToString();
                } catch (Exception ex)
                 {
                     if (ex is RuntimeBinderException || ex is InvalidOperationException)
                     {
                    phone= "null";
                    }
                }

                string phone2 = "";
                try
                {
                    phone2 = data["Contact"]["phone2"].ToString();
                } catch (Exception ex)
                 {
                     if (ex is RuntimeBinderException || ex is InvalidOperationException)
                     {
                    phone2= "null";
                    }
                }

                string fax = "";
                try
                {
                    fax = data["Contact"]["fax"].ToString();
                } catch (Exception ex)
                 {
                     if (ex is RuntimeBinderException || ex is InvalidOperationException)
                     {
                    fax= "null";
                    }
                }

                string independent = "";
                if (dataReader["independent"].ToString() != "")
                {
                    independent = dataReader["independent"].ToString();
                } else {
                    independent = "null";
                }
                try
                {
                    independent = data["Contact"]["independent"].ToString();
                } catch (Exception ex)
                 {
                     if (ex is RuntimeBinderException || ex is InvalidOperationException)
                     {
                    independent= "null";
                    }
                }

                string dancer_id = "";
                try
                {
                    dancer_id = data["Contact"]["regprofileid"].ToString();
                } catch (Exception ex)
                 {
                     if (ex is RuntimeBinderException || ex is InvalidOperationException)
                     {
                    dancer_id= "null";
                    }
                }
                string payment_method = "";
                try
                {
                    payment_method = data["Payment"]["payment_method"].ToString();
                } catch (Exception ex)
                 {
                     if (ex is RuntimeBinderException || ex is InvalidOperationException)
                     {
                    payment_method= "null";
                    }
                }
                string studioname = "";
                try
                {
                    studioname = data["Contact"]["studioname"].ToString();
                    studioname = studioname.Replace("'","''").Replace("\\", "");

                } catch (Exception ex)
             {
                 if (ex is RuntimeBinderException || ex is InvalidOperationException)
                 {
                 studioname= "null";
                 }
                }
                string studioowner = "";
                try
                {
                    studioowner = data["Contact"]["studioowner"].ToString();
                    studioowner = studioowner.Replace("'","''").Replace("\\", "");
                } catch (Exception ex)
             {
                 if (ex is RuntimeBinderException || ex is InvalidOperationException)
                 {
                    studioowner= "null";
                    }
                }
                string organization = "";
                try
                {
                    organization = data["Contact"]["organization"].ToString();
                    organization = organization.Replace("'", "''").Replace("\\", "");
                }
                catch (Exception ex)
                {
                    if (ex is RuntimeBinderException || ex is InvalidOperationException)
                    {
                        organization = "null";
                    }
                }


                pPostgres.Insert("insert into tbl_registrations(id, tour_date_id, studio_id, dates, completed, confirmed, heard, details, entered_by_id, viewed, deleted, payment_method_id, independent) " +
                                 "values('" + dataReader["id"] + "','" + dataReader["tourdateid"] + "'," + GetStudioId(dataReader["studio"].ToString().Replace("'","''").Replace("\\",""), dataReader["organization"].ToString().Replace("\\", ""), studioname, studioowner, organization, pPostgres) + "," +
                                 "'" + Get_json_date(CheckIfExists(dataReader["date"]), CheckIfExists(dataReader["confirmdate"])) + "','1'," + CheckBool(dataReader["confirmed"].ToString()) + "," +
                                 "" + NVLNULL(dataReader["heard"].ToString().Replace("'", "''")) + "," +
                                 "" + NVLNULL(dataReader["details"].ToString().Replace("'", "''")) + "," + NVL(dataReader["enteredby"].ToString()) + ", " + CheckBool(dataReader["viewed"].ToString()) + ", " +
                                 "" + CheckBool(dataReader["deleted"].ToString()) + "," + GetPaymentMethodId(NVLJSON(dataReader["payment_method"].ToString()), NVLJSON(payment_method)) + ", "+NVLNULL(independent)+");");
            }
            pPostgres.Insert("insert into tbl_registrations(id) values(22996);");
            pPostgres.Insert("insert into tbl_registrations(id) values(23035);");
            pPostgres.Insert("insert into tbl_registrations(id) values(23127);");
            pPostgres.Insert("insert into tbl_registrations(id) values(24225);");
            pPostgres.Insert("insert into tbl_registrations(id) values(31298);");

            pPostgres.Insert("insert into tbl_registrations(id) values(26215);");
            pPostgres.Insert("insert into tbl_registrations(id) values(35655);");
            pPostgres.Insert("insert into tbl_registrations(id) values(36643);");
            pPostgres.Insert("insert into tbl_registrations(id) values(43367);");

            pPostgres.Message = "tbl_registrations (dancetea.registration) - extraction - FINISH";
        }
        public string Get_address(string address, string city, string state, string zip, string countryid, PostgreSQL_DB pg)
        {
            string existingAddressId = GetId("select id from tbl_addresses where address ilike '"+address.Replace("'", "''")+"' and city ilike '"+city.Replace("'", "''")+"';", pg);
            if (existingAddressId == "null")
            {
                string pomStateId = GetId("select id from tbl_states where name ilike '" + state + "'", pg);
                int maxAddressId = Convert.ToInt32(GetId("select max(id) from tbl_addresses", pg));
                pg.Insert("insert into tbl_addresses(id, address, city, state_id, zip, country_id) " + "values('" + ++maxAddressId + "'," + NVLNULL(address) + "," + NVLNULL(city) + "," + NVLNULL(pomStateId) + "," + NVLNULL(zip) + "," + NVLNULL(countryid) + ");")
                ; existingAddressId = maxAddressId.ToString();
            }
            return existingAddressId;
        }



        private string CheckIfExists(object data)
        {
            if (data.GetType().Name == "DBNull" || data == null)
            {
                return "null";
            }
            else
            {
                return FromUnixTime(Convert.ToInt64(data)).ToString().Replace(". ", ".");
            }
        }

        public string GetPaymentMethodId(string paymentMethod, string paymentMethod2)
        {
            if (paymentMethod2 != "" && paymentMethod2 != "null")
            {
                if (paymentMethod2 == "credit_card")
                {
                    return "1";
                }
                if (paymentMethod2 == "check")
                {
                    return "2";
                }
                if (paymentMethod2 == "nobal")
                {
                    return "3";
                }
            }
            if (paymentMethod != "" && paymentMethod != "null")
            {
                if (paymentMethod == "credit_card")
                {
                    return "1";
                }
                if (paymentMethod == "check")
                {
                    return "2";
                }
                if (paymentMethod == "nobal")
                {
                    return "3";
                }
            }

            return "null";
        }

        public string GetStudioId(string pStudio_name, string organization, string studioname, string studioowner, string organization2, PostgreSQL_DB pPostgres)
        {
            if (studioname != "" && studioname != "null" && studioname != null)
            {
                string studioId = GetId("select id from tbl_studios where name like '" + studioname + "'", pPostgres);
                if (studioId != null && studioId != "null" && studioId != "")
                {
                    return studioId;
                }
                else
                {
                    if (studioowner != "null" && studioowner != "")
                    {
                        studioowner = GetStudioOwner(studioowner, pPostgres);
                    }
                    return AddNewStudio(studioname, studioowner, pPostgres);
                }
            }
            if (organization2 != "" && organization2 != "null" && organization2 != null)
            {
                string studioId = GetId("select id from tbl_studios where name like '" + organization2 + "'", pPostgres);
                if (studioId != null && studioId != "null" && studioId != "")
                {
                    return studioId;
                }
                else
                {
                    if (studioowner != "null" && studioowner != "")
                    {
                        studioowner = GetStudioOwner(studioowner, pPostgres);
                    }
                    return AddNewStudio(organization2, studioowner, pPostgres);
                }
            }
            if (organization != "" && organization != "null" && organization != null)
            {
                string studioId = GetId("select id from tbl_studios where name like '" + organization + "'", pPostgres);
                if (studioId != null && studioId != "null" && studioId != "")
                {
                    return studioId;
                }
                else
                {
                    if (studioowner != "null" && studioowner != "")
                    {
                        studioowner = GetStudioOwner(studioowner, pPostgres);
                    }
                    return AddNewStudio(organization, studioowner, pPostgres);
                }
            }

            if (pStudio_name == "null" || pStudio_name == "")
            {
                return "null";
            }
            else
            {
                string studioId = GetId("select id from tbl_studios where name like '" + pStudio_name + "'", pPostgres);
                if (studioId != null && studioId != "null" && studioId != "")
                {
                    return studioId;
                }
                else
                {
                    if (studioowner != "null" && studioowner != "")
                    {
                        studioowner = GetStudioOwner(studioowner, pPostgres);
                    }
                    return AddNewStudio(pStudio_name, studioowner, pPostgres);
                }
            }
        }

        private string Get_json_date(string pFirst_date, string pSecond_date)
        {
            dynamic date = new JObject();
            if (pFirst_date == "null" || pFirst_date == "")
            {
                date.completed = null;
            } else
            {
                date.completed = pFirst_date;

            }
            if (pSecond_date == "null" || pSecond_date == "")
            {
                date.confirmed = null;
            } else
            {
                date.confirmed = pSecond_date;

            }
            return date.ToString();
        }
        public string GetStudioOwner(string studioowner, PostgreSQL_DB pPostgres)
        {
            dynamic owner = new JObject();
            owner.person_type_id = 3;
            string[] splitstudioowner = studioowner.Split(' ');
            if (splitstudioowner.Length == 1)
            {
                owner.lname = splitstudioowner[0];
                owner.fname = "null";
            }
            if (splitstudioowner.Length > 1)
            {
                owner.fname = splitstudioowner[0];
                owner.lname = splitstudioowner[1];
            }
            pPostgres.Insert("insert into tbl_persons(person_type_id, fname, lname) values(" + owner.person_type_id.ToString() + ","+NVLNULL(owner.fname.ToString())+","+NVLNULL(owner.lname.ToString())+")");
            string person_id = GetId("select max(id) from tbl_persons;", pPostgres);
            return person_id;
        }
    }
}