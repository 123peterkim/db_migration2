﻿using System;
using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;
using Npgsql;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_dancers_II : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            // all dancers related to the any studio...
            MySqlDataReader dataReader = pMysql.Select("select * from tbl_profiles where studioid is null;");
            pMysql.Message = "tbl_dancers II. - extraction - START";
            while (dataReader.Read())
            {
                // insert new address of dancer (person)
                // private string GetAddressId(string pAddress, string pCity, string pState, string pZip, string pCountryId, PostgreSQL_DB pPostgres)
                string AddressId = GetAddressId(dataReader["address"].ToString(), dataReader["city"].ToString(),
                    dataReader["state"].ToString(), dataReader["zip"].ToString(), dataReader["countryid"].ToString(),
                    pPostgres);
                string GenderId = GetId("select id from tbl_gender where value like '" + dataReader["gender"] + "'", pPostgres);

                // inser dancer into person
                string birthdate;
                if (dataReader["birth_date"].ToString().Equals("") || dataReader["birth_date"].ToString().Equals("null"))
                {
                    birthdate = "null";
                }
                else
                {
                    birthdate = FromDashDateToSlash(dataReader["birth_date"].ToString());
                }
                pPostgres.Insert("insert into tbl_persons(address_id, gender_id, fname, lname, birthdate, person_type_id) " +
                                 "values(" + AddressId + ", " + GenderId + ",'" + dataReader["fname"].ToString().Replace("'", "''") + "'," +
                                 "'" + dataReader["lname"].ToString().Replace("'", "''") + "'," + birthdate + ",'8')");
                string Max_person_id = GetId("select max(id) from tbl_persons", pPostgres);

                // insert into tbl_dancers
                string parent_guardian = (dataReader["parent_guardian"].ToString() == "") ? "null" : dataReader["parent_guardian"].ToString();
                string email_parents = (dataReader["email_parents"].ToString() == "") ? "null" : dataReader["email_parents"].ToString();
                if (email_parents.Equals("''"))
                {
                    email_parents = "null";
                }
                pPostgres.Insert("insert into tbl_dancers(id, person_id, parent_guardian, email_parents) " +
                                 "values('" + dataReader["id"] + "'," + Max_person_id + ",'" + parent_guardian.Replace("'", "''") + "','" + email_parents.Replace("'", "''") + "')");

                // insert into studio_has_contact_type
                if (dataReader["phone"].ToString() != "")
                {
                    pPostgres.Insert("insert into person_contact_info(person_id, contact_type_id, value)" +
                                     "values(" + Max_person_id + ",2,'" + dataReader["phone"] + "')");
                }
                if (dataReader["phone2"].ToString() != "")
                {
                    pPostgres.Insert("insert into person_contact_info(person_id, contact_type_id, value)" +
                                     "values(" + Max_person_id + ",2,'" + dataReader["phone2"] + "')");
                }
                if (dataReader["fax"].ToString() != "")
                {
                    pPostgres.Insert("insert into person_contact_info(person_id, contact_type_id, value)" +
                                     "values(" + Max_person_id + ",8,'" + dataReader["fax"] + "')");
                }
                if (dataReader["email"].ToString() != "")
                {
                    pPostgres.Insert("insert into person_contact_info(person_id, contact_type_id, value)" +
                                     "values(" + Max_person_id + ",1,'" + dataReader["email"].ToString().ToLower().Replace("'", "''") + "')");
                }
            }
            pPostgres.Message = "tbl_dancers II. - extraction - FINISH";
        }
        private string GetAddressId(string pAddress, string pCity, string pState, string pZip, string pCountryId, PostgreSQL_DB pPostgres)
        {
            NpgsqlDataReader query;
            query = pPostgres.Select("select distinct id " +
                                   "from tbl_addresses where address like '" + pAddress.Replace("'", "''") + "' and city = '" + pCity.Replace("'", "''") + "' and zip like '" + pZip + "' and country_id='" + pCountryId + "';");
            string pom;
            while (query.Read())
            {
                pom = query[0].ToString();
                query.Dispose();
                return "'" + pom + "'";
            }
            query.Dispose();
            if (pAddress != "")
            {
                string pomStateId = GetId("select id from tbl_states where name like '" + pState + "'", pPostgres);
                pPostgres.Insert("insert into tbl_addresses(state_id, address, city, zip) values(" + pomStateId + ",'" + pAddress.Replace("'", "''") + "','" + pCity.Replace("'", "''") + "','" + pZip + "');");
                string p_address_id = GetId("select max(id) from tbl_addresses", pPostgres);
                return p_address_id;
            }
            else
            {
                return "null";
            }
        }
    }
}