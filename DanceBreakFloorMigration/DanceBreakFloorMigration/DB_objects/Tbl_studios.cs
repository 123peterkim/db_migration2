﻿using System;
using System.Linq;
using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;
using Npgsql;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_studios : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from tbl_studios;");
            pMysql.Message = "tbl_studios - extraction - START";
            while (dataReader.Read())
            {
                string notes = (dataReader["notes"].ToString() == "") ? "null" : "'" + dataReader["notes"].ToString().Replace(Environment.NewLine, "").Replace("\r\n", "").Replace("\r", "").Replace("\n", "") + "'";

                pPostgres.Insert("insert into tbl_studios(id, name, notes, address_id) " +
                                "values('" + dataReader["id"] + "','" + dataReader["name"].ToString().Replace("'", "''") + "',"+notes+ ","+GetAddressId(dataReader["address"].ToString(), dataReader["city"].ToString(), dataReader["state"].ToString(),
                                 dataReader["zip"].ToString(), dataReader["countryid"].ToString(), pPostgres) + ");");

                InsertContacts(dataReader["phone"].ToString(), dataReader["phone2"].ToString(), dataReader["fax"].ToString(), dataReader["email"].ToString(), dataReader["id"].ToString(), pPostgres);
                if (dataReader["contacts"].ToString() != "") {
                    string contacts = dataReader["contacts"].ToString().Replace("'", "''").Replace(Environment.NewLine, "").Replace("\r\n", "").Replace("\r", "").Replace("\n", "");
                    dynamic contacts_object = JsonConvert.DeserializeObject<dynamic>(contacts);

                    foreach (dynamic contact in contacts_object)
                    {
                        if (contact.fname.ToString() != "" && contact.lname.ToString() != "")
                        {
                            InsertPersons(contact, dataReader["id"].ToString(), pPostgres);
                        }
                    }
                }


            }
            pPostgres.Message = "tbl_studios - extraction - FINISH";
            DummyStudioCreation(pMysql, pPostgres);
            ;
        }

        private string GetAddressId(string pAddress, string pCity, string pState, string pZip, string pCountryId, PostgreSQL_DB pPostgres)
        {
            NpgsqlDataReader query;
            query = pPostgres.Select("select distinct id " +
                                   "from tbl_addresses where address like '" + pAddress.Replace("'", "''") + "' and city = '" + pCity.Replace("'", "''") + "' and zip like '"+pZip+"' and country_id='"+pCountryId+"';");
            string pom;
            while (query.Read())
            {
                pom = query[0].ToString();
                query.Dispose();
                return "'" + pom + "'";
            }
            query.Dispose();
            if (pAddress != "")
            {
                string pomStateId = GetId("select id from tbl_states where name like '" + pState + "'", pPostgres);
                pPostgres.Insert("insert into tbl_addresses(state_id, address, city, zip, country_id) values(" + pomStateId + ",'" + pAddress.Replace("'", "''") + "','" + pCity.Replace("'", "''") + "','" + pZip + "','"+pCountryId+"');");
                string p_address_id = GetId("select max(id) from tbl_addresses", pPostgres);
                return p_address_id;
            }
            else
            {
                return "null";
            }
        }
        public static new string Remove(string source, char[] oldChar)
        {
            return String.Join("", source.ToCharArray().Where(a => !oldChar.Contains(a)).ToArray());
        }

        private void InsertContacts(string pPhone, string pPhone2, string pFax, string pEmail, string pStudioId, PostgreSQL_DB pPostgres)
        {
            var c = new[] { '(', ')', '-', ' ' };

            if (pPhone != "")
                pPostgres.Insert("insert into studio_contact_info(contact_type_id, studio_id, value) values('" + 2 + "','" + pStudioId + "','" + Remove(pPhone, c) + "');");
            if (pPhone2 != "")
                pPostgres.Insert("insert into studio_contact_info(contact_type_id, studio_id, value) values('" + 2 + "','" + pStudioId + "','" + Remove(pPhone2, c) + "');");
            if (pFax != "")
                pPostgres.Insert("insert into studio_contact_info(contact_type_id, studio_id, value) values('" + 8 + "','" + pStudioId + "','" + Remove(pFax, c) + "');");
            if (pEmail != "")
                pPostgres.Insert("insert into studio_contact_info(contact_type_id, studio_id, value) values('" + 1 + "','" + pStudioId + "','" + pEmail.Replace("'","''") + "');");
        }

        private void InsertPersons(dynamic contact, string id, PostgreSQL_DB pPostgres)
        {
            dynamic person = new JObject();
            person.fname = contact.fname;
            person.lname = contact.lname;
            person.person_type_id = "null";
            if (contact.type.ToString() == "Teacher")
            {
                person.person_type_id = "2";
            }
            if (contact.type.ToString() == "Studio Owner")
            {
                person.person_type_id = "3";
            }
            if (contact.type.ToString() == "Office Manager")
            {
                person.person_type_id = "5";
            }
            if (contact.type.ToString() == "Dancer")
            {
                person.person_type_id = "8";
            }
            if (contact.type.ToString() == "Parent")
            {
                person.person_type_id = "9";
            }
            pPostgres.Insert("insert into tbl_persons(person_type_id, fname, lname) values(" + person.person_type_id.ToString() + "," + NVLNULL(person.fname.ToString()) + "," + NVLNULL(person.lname.ToString()) + ");");
            string PersonId = GetId("select max(id) from tbl_persons", pPostgres);
            pPostgres.Insert("insert into studios_persons(person_id, studio_id) values(" + PersonId + "," + id + ");");
        }

        private void DummyStudioCreation(MySQL_DB pMysql, PostgreSQL_DB pPostgres)
        {
            MySqlDataReader dataReader = pMysql.Select("select distinct studioid from tbl_profiles where studioid not in (select id from tbl_studios);");
            pMysql.Message = "DUMMY tbl_studios - extraction - START";
            while (dataReader.Read())
            {
                pPostgres.Insert("insert into tbl_studios(id, name) values('"+dataReader[0]+"','DUMMY DANCE STUDIO')");

            }
            pPostgres.Message = "DUMMY tbl_studios - extraction - FINISH";
        }
    }
}