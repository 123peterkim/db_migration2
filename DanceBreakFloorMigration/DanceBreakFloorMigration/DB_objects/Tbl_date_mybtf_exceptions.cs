﻿using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Date_mybtf_exceptions : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from tbl_date_mybtf_exceptions;");
            pMysql.Message = "date_mybtf_exceptions - extraction - START";
            while (dataReader.Read())
            {
                pPostgres.Insert("insert into date_mybtf_exceptions(id, tour_date_id, email, level) " +
                                 "values('"+dataReader["id"]+ "','" + dataReader["tourdateid"] + "','" + dataReader["email"] + "','" + dataReader["level"] + "')");
            }
            pPostgres.Message = "date_mybtf_exceptions - extraction - FINISH";
        }
    }
}