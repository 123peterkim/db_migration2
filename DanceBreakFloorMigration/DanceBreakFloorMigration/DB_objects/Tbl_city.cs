﻿using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_city: BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            pMysql.Message = "tbl_vip_type - extraction - START";

            pPostgres.Insert("insert into tbl_vip_types(id, name) values(1,'JUMP VIP')");
            pPostgres.Insert("insert into tbl_vip_types(id, name) values(2,'NUVO BreakOut')");
            pPostgres.Insert("insert into tbl_vip_types(id, name) values(3,'24 Seven Non-Stop Dancer')");
            pPostgres.Message = "tbl_vip_type - extraction - FINISH";
        }
    }
}