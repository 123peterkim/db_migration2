﻿using System;
using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_mybtf_user_has_dancers: BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select distinct userid, profileid from tbl_saved_dancers;");
            pMysql.Message = "tbl_mybtf_user_has_dancers - extraction - START";
            while (dataReader.Read())
            {
                 pPostgres.Insert("insert into tbl_mybtf_user_has_dancers(dancer_id, mybtf_user_id) values('" + dataReader["profileid"] + "','" + dataReader["userid"] + "')");
            }
            pPostgres.Message = "tbl_mybtf_user_has_dancers - extraction - FINISH";
        }
    }
}