﻿using System;
using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_tda_award_nominations : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from tbl_tda_award_nominations");
            pMysql.Message = "tbl_tda_award_nominations - extraction - START ";
            while (dataReader.Read())
            {
                string pOacdDesigner = "0";
                string pTsChoreographer = "0";
                string pMjChoreographer = "0";
                if (!String.IsNullOrEmpty(dataReader["sa_oacd_designer"].ToString()))
                {
                    pOacdDesigner = AddNewPerson(dataReader["sa_oacd_designer"].ToString(), "12", pPostgres);
                }
                if (!String.IsNullOrEmpty(dataReader["sa_ts_bc_choreographer"].ToString()))
                {
                    pTsChoreographer = AddNewPerson(dataReader["sa_ts_bc_choreographer"].ToString(), "2", pPostgres);
                }
                if (!String.IsNullOrEmpty(dataReader["sa_mj_bc_choreographer"].ToString()))
                {
                    pMjChoreographer = AddNewPerson(dataReader["sa_mj_bc_choreographer"].ToString(), "2", pPostgres);
                }

                pPostgres.Insert("insert into tbl_tda_award_nominations(id, studio_id, tour_date_id, has_soty, soty_ts_ballet_routine_id, " +
                                 "soty_ts_jazz_routine_id, soty_ts_musicaltheater_routine_id, soty_ts_contemplyrical_routine_id, soty_ts_hiphoptap_routine_id, " +
                                 "soty_mj_ballet_routine_id, soty_mj_jazz_routine_id, soty_mj_musicaltheater_routine_id, soty_mj_contemplyrical_routine_id, " +
                                 "soty_mj_hiphoptap_routine_id, sa_ts_ota_routine_id, sa_ts_ota_no, sa_mj_ota_routine_id, sa_mj_ota_no, sa_oacd_routine_id, " +
                                 "sa_oacd_designer_id, sa_oacd_no, sa_ts_bc_routine_id, sa_ts_bc_no, sa_mj_bc_routine_id, sa_mj_bc_no, sa_peopleschoice_no, " +
                                 "sa_peopleschoice_routine_id, sa_s_ota_routine_id, sa_s_ota_no, sa_t_ota_routine_id, sa_t_ota_no, sa_j_ota_routine_id, " +
                                 "sa_j_ota_no, sa_m_ota_routine_id, sa_m_ota_no, sa_ts_bc_choreographer_id, sa_mj_bc_choreographer_id) " +
                                 "values(" + dataReader["id"] + "," + dataReader["studioid"] + "," + dataReader["tourdateid"] + "," +
                                 "" + CheckBool(dataReader["has_soty"].ToString()) + "," + NVL(dataReader["soty_ts_ballet_routineid"].ToString()) + "," + NVL(dataReader["soty_ts_jazz_routineid"].ToString()) + "," +
                                 "" + NVL(dataReader["soty_ts_musicaltheater_routineid"].ToString()) + "," + NVL(dataReader["soty_ts_contemplyrical_routineid"].ToString()) + "," +
                                 "" + NVL(dataReader["soty_ts_hiphoptap_routineid"].ToString()) + "," + NVL(dataReader["soty_mj_ballet_routineid"].ToString()) + "," + NVL(dataReader["soty_mj_jazz_routineid"].ToString()) + "," +
                                 "" + NVL(dataReader["soty_mj_musicaltheater_routineid"].ToString()) + "," + NVL(dataReader["soty_mj_contemplyrical_routineid"].ToString()) + "," +
                                 "" + NVL(dataReader["soty_mj_hiphoptap_routineid"].ToString()) + "," + NVL(dataReader["sa_ts_ota_routineid"].ToString()) + "," + CheckBool(dataReader["sa_ts_ota_no"].ToString()) + "," +
                                 "" + NVL(dataReader["sa_mj_ota_routineid"].ToString()) + "," + CheckBool(dataReader["sa_mj_ota_no"].ToString()) + "," + NVL(dataReader["sa_oacd_routineid"].ToString()) + "," +
                                 "" + NVLZERO(pOacdDesigner) + "," + CheckBool(dataReader["sa_oacd_no"].ToString()) + "," + NVL(dataReader["sa_ts_bc_routineid"].ToString()) + "," +
                                 "" + CheckBool(dataReader["sa_ts_bc_no"].ToString()) + "," + NVL(dataReader["sa_mj_bc_routineid"].ToString()) + "," + CheckBool(dataReader["sa_mj_bc_no"].ToString()) + "," + CheckBool(dataReader["sa_peopleschoice_no"].ToString()) + "," +
                                 "" + NVL(dataReader["sa_peopleschoice_routineid"].ToString()) + "," + NVL(dataReader["sa_s_ota_routineid"].ToString()) + "," + CheckBool(dataReader["sa_s_ota_no"].ToString()) + "," +
                                 "" + NVL(dataReader["sa_t_ota_routineid"].ToString()) + "," + CheckBool(dataReader["sa_t_ota_no"].ToString()) + "," + NVL(dataReader["sa_j_ota_routineid"].ToString()) + "," +CheckBool(dataReader["sa_j_ota_no"].ToString()) + "," +
                                 "" + NVL(dataReader["sa_m_ota_routineid"].ToString()) + "," + CheckBool(dataReader["sa_m_ota_no"].ToString()) + "," + NVLZERO(pTsChoreographer) + "," + NVLZERO(pMjChoreographer) + ");");
                
            }

            pPostgres.Message = "tbl_tda_award_nominations - extraction - FINISH";
        }
        public string AddNewPerson(string pName, string pType, PostgreSQL_DB pPostgres)
        {
            string personName = pName.ToString();
            int idx = personName.LastIndexOf(' ');
            string PersonType = pType;
            int Max_person_id = Convert.ToInt32(GetId("select max(id) from tbl_persons", pPostgres));

            // string PersonType = GetId("select id from tbl_person_types where name like 'Costume Designer' limit 1;", pPostgres);
            if (idx != -1)
            {
                pPostgres.Insert("insert into tbl_persons(id, fname, lname, person_type_id) " +
                              "values("+ ++Max_person_id +",'" + personName.Substring(0, idx).Replace("'","''") + "','" + personName.Substring(idx + 1).Replace("'","''") + "'," + PersonType + ");");
            } else
            {
                pPostgres.Insert("insert into tbl_persons(id, fname, lname, person_type_id) " +
                              "values("+ ++Max_person_id +",'" + personName.Replace("'","''") + "','" + "null" + "'," + PersonType + ");");
            }
            
            return Max_person_id.ToString();
        }
    }    
}