﻿using System;
using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Net;
using Npgsql;
using Microsoft.VisualBasic;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_schedule_workshops_room : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select * from tbl_date_schedule_workshops");
            pMysql.Message = "date_schedule_workshop_rooms - extraction - START ";
            while (dataReader.Read())
            {
                if (dataReader["room1"].ToString()!="")
                {
                    pPostgres.Insert("insert into date_schedule_workshop_rooms(number, date_schedule_workshop_id, name, bold, highlight) " +
                                    "values(1," + dataReader["id"] + ", '" + WebUtility.UrlDecode(dataReader["room1"].ToString()).Replace("'", "''") + "','" + CheckBool(dataReader["room1_bold"].ToString()) + "','" + CheckBool(dataReader["room1_highlight"].ToString()) + "')");
                }
                if (dataReader["room2"].ToString() != "")
                {
                    pPostgres.Insert("insert into date_schedule_workshop_rooms(number, date_schedule_workshop_id, name, bold, highlight) " +
                                    "values(2," + dataReader["id"] + ", '" + WebUtility.UrlDecode(dataReader["room2"].ToString()).Replace("'", "''") + "','" + CheckBool(dataReader["room2_bold"].ToString()) + "','" + CheckBool(dataReader["room2_highlight"].ToString()) + "')");
                }
                if (dataReader["room3"].ToString() != "")
                {
                    pPostgres.Insert("insert into date_schedule_workshop_rooms(number, date_schedule_workshop_id, name, bold, highlight) " +
                                    "values(3," + dataReader["id"] + ", '" + WebUtility.UrlDecode(dataReader["room3"].ToString()).Replace("'", "''") + "','" + CheckBool(dataReader["room3_bold"].ToString()) + "','" + CheckBool(dataReader["room3_highlight"].ToString()) + "')");
                }
                if (dataReader["room4"].ToString() != "")
                {
                    pPostgres.Insert("insert into date_schedule_workshop_rooms(number, date_schedule_workshop_id, name, bold, highlight) " +
                                    "values(4," + dataReader["id"] + ", '" + WebUtility.UrlDecode(dataReader["room4"].ToString()).Replace("'", "''") + "','" + CheckBool(dataReader["room4_bold"].ToString()) + "','" + CheckBool(dataReader["room4_highlight"].ToString()) + "')");
                }
                if (dataReader["room5"].ToString() != "")
                {
                    pPostgres.Insert("insert into date_schedule_workshop_rooms(number, date_schedule_workshop_id, name, bold, highlight) " +
                                    "values(5," + dataReader["id"] + ", '" + WebUtility.UrlDecode(dataReader["room5"].ToString()).Replace("'", "''") + "','" + CheckBool(dataReader["room5_bold"].ToString()) + "','" + CheckBool(dataReader["room5_highlight"].ToString()) + "')");
                }
                if (dataReader["room6"].ToString() != "")
                {
                    pPostgres.Insert("insert into date_schedule_workshop_rooms(number, date_schedule_workshop_id, name, bold, highlight) " +
                                    "values(6," + dataReader["id"] + ", '" + WebUtility.UrlDecode(dataReader["room6"].ToString()).Replace("'", "''") + "','" + CheckBool(dataReader["room6_bold"].ToString()) + "','" + CheckBool(dataReader["room6_highlight"].ToString()) + "')");
                }
                if (dataReader["room7"].ToString() != "")
                {
                    pPostgres.Insert("insert into date_schedule_workshop_rooms(number, date_schedule_workshop_id, name, bold, highlight) " +
                                    "values(7," + dataReader["id"] + ", '" + WebUtility.UrlDecode(dataReader["room7"].ToString()).Replace("'", "''") + "','" + CheckBool(dataReader["room7_bold"].ToString()) + "','" + CheckBool(dataReader["room7_highlight"].ToString()) + "')");
                }
            }
            pPostgres.Message = "date_schedule_workshop_rooms - extraction - FINISH";
        }
    }
}