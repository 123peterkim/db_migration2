﻿using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using Microsoft.CSharp.RuntimeBinder;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Web.Script.Serialization;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_online_scoring_has_staff : BaseClass, IMigration
    {
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select id, facultyid1 as faculty, data1 as data from tbl_online_scoring where facultyid1!=0 " +
                                                       "union select id, facultyid2 as faculty, data2 as data from tbl_online_scoring where facultyid2!=0 " +
                                                       "union select id, facultyid3 as faculty, data3 as data from tbl_online_scoring where facultyid3!=0 " +
                                                       "union select id, facultyid4 as faculty, data4 as data from tbl_online_scoring where facultyid4!=0");

            pMysql.Message = "tbl_online_scoring_data - extraction - START";
            int counter = 0;
            while (dataReader.Read())
            {
                string note = "null";
                string score = "null";
                string attributes = "null";
                string not_friendly = "null";
                string i_choreographed = "null";

                if (dataReader["data"].ToString().Replace("'","''") != "")
                {
                    dynamic item = JsonConvert.DeserializeObject<dynamic>(dataReader["data"].ToString());
                    try
                    {
                        note = (item["note"].ToString() == "") ? "null" : "'" + item["note"].ToString().Replace(Environment.NewLine, "").Replace("\r\n", "").Replace("\r", "").Replace("\n", "").Replace("'", "''") + "'";

                    } catch (Exception ex)
                    {
                        if (ex is RuntimeBinderException || ex is InvalidOperationException)
                        {
                            note = "null";
                        }
                    }
                    try
                    {
                        score = (item["score"].ToString() == "") ? "null" : item["score"].ToString();
                    } catch (Exception ex)
                    {
                        if (ex is RuntimeBinderException || ex is InvalidOperationException)
                        {
                        score = "null";
                        }
                    }
                    try
                    {
                        attributes = (item["attributes"].ToString() == "") ? "null" : "'" + item["attributes"].ToString().Replace(Environment.NewLine, "").Replace("\r\n", "").Replace("\r", "").Replace("\n", "").Replace("'", "''") + "'";
                    } catch (Exception ex)
                     {
                         if (ex is RuntimeBinderException || ex is InvalidOperationException)
                         {
                        attributes = "null";
                        }
                    }
                    try
                    {
                        not_friendly = (item["not_friendly"].ToString() == "") ? "null" : item["not_friendly"].ToString();
                    } catch (Exception ex)
                     {
                         if (ex is RuntimeBinderException || ex is InvalidOperationException)
                         {
                        not_friendly = "null";
                        }
                    }
                    try
                    {
                        i_choreographed = (item["i_choreographed"].ToString() == "") ? "null" : item["i_choreographed"].ToString();
                    } catch (Exception ex)
                 {
                     if (ex is RuntimeBinderException || ex is InvalidOperationException)
                     {
                        i_choreographed = "null";
                        }
                    }
                }

                pPostgres.Insert("insert into tbl_online_scoring_data(id, online_scoring_id, staff_id, note, score, data, not_friendly, i_choreographed) " +
                                 "values('"+ ++counter +"','"+dataReader["id"]+"','"+dataReader["faculty"]+"',"+ note + ","+score+","+attributes+","+not_friendly+","+i_choreographed+")");
            }
            pPostgres.Message = "tbl_online_scoring_data - extraction - FINISH";
        }

    }
}