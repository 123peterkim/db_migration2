﻿using System.Runtime.InteropServices;
using DanceBreakFloorMigration.Classes;
using DanceBreakFloorMigration.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace DanceBreakFloorMigration.DB_objects
{
    public class Tbl_score_colors: BaseClass, IMigration
    {
        public void SupRemigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
                {            
                    MySqlDataReader dataReader = pMysql.Select("select id, name, update_date, insert_date from store_colors;");
        
                    pMysql.Message = "tbl_store_colors - extraction - START";
                    string pom1 = "null";
                    string pom2 = "null";
                    while (dataReader.Read())
                    {
                        // update part start HERE
                        if (!String.IsNullOrEmpty(dataReader["update_date"].ToString()) && Convert.ToDateTime(dataReader["update_date"].ToString()) >= Convert.ToDateTime(pDate))
                        {                    
                            List<string> MySQLData = new List<string> {"null", dataReader["id"].ToString(), dataReader["name"].ToString()};
                            List<string> PostgreSQLData = new List<string> {"tbl_store_colors", "id", "name"};
                            UpdatePostgresRow(dataReader["id"].ToString(), MySQLData, PostgreSQLData,  pPostgres);
                        }

                        if (GetId("select id from tbl_store_colors where id = " + dataReader["id"] + ";", pPostgres) == "null" && !String.IsNullOrEmpty(dataReader["insert_date"].ToString()) && Convert.ToDateTime(dataReader["insert_date"].ToString()) >= Convert.ToDateTime(pDate))
                                        {
                                            pPostgres.Insert("insert into tbl_store_colors(id, name) values(" + (int)dataReader[0] + ",'" + dataReader[1] + "')");
                                        }
                        // update part END
                    }
        
                    pPostgres.Message = "tbl_store_colors - extraction - FINISH";
                }
        public void Remigration(MySQL_DB pMysql, PostgreSQL_DB pPostgres, string pDate = "1.1.2500")
        {
            MySqlDataReader dataReader = pMysql.Select("select id, name from store_colors;");
            pMysql.Message = "store_colors - extraction - START";
            while (dataReader.Read())
            {
                pPostgres.Insert("insert into tbl_store_colors(id, name) values(" + (int)dataReader[0] + ",'" + dataReader[1] + "')");
            }
            pPostgres.Message = "store_colors - extraction - FINISH";
        }
    }
}